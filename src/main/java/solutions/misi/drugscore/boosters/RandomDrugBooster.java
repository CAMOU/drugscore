package solutions.misi.drugscore.boosters;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.drugs.Drug;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomDrugBooster extends ItemStack implements Listener {

    @Getter final String displayName;

    public RandomDrugBooster() {
        final Material material = Material.END_CRYSTAL;
        displayName = "§5Random Drug Booster";
        final List<String> lore = new ArrayList<>();
        lore.add("§b§oApplies a drug selling boost of §lx2");
        lore.add("§b§oto a Random drug for 15m!");

        this.setType(material);
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(displayName);
        itemMeta.setLore(lore);
        this.setItemMeta(itemMeta);
    }

    public static void activate(Player player, ItemStack booster) {
        Drug boostedDrug;

        while(true) {
            Random random = new Random();
            int randomDrugIndex = random.nextInt(DrugsCore.getInstance().getDrugsManager().drugs.size());
            Drug randomDrug = DrugsCore.getInstance().getDrugsManager().drugs.get(randomDrugIndex);

            if(!DrugsCore.getInstance().getShopBoosterHandler().getActiveRandomBoosters().containsKey(randomDrug)) {
                DrugsCore.getInstance().getShopBoosterHandler().getActiveRandomBoosters().put(randomDrug, System.currentTimeMillis()+900000);
                boostedDrug = randomDrug;
                break;
            }
        }

        DrugsCore.getInstance().getSchedulers().startRandomDrugBoosterTask(boostedDrug);

        booster.setAmount(booster.getAmount() - 1);
        Bukkit.broadcastMessage(DrugsCore.getInstance().getMessage().getBoosterActivated().replace("[type]", "Random").replace("[drug]", boostedDrug.getName()).replace("[player]", player.getDisplayName()).replace("[time]", "15min"));
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        try {
            if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(displayName)) {
                if(event.getHand() == EquipmentSlot.HAND) {
                    DrugsCore.getInstance().getConfirmGUI().open(player, displayName);
                }
            }
        } catch(NullPointerException ex) {
        }
    }
}