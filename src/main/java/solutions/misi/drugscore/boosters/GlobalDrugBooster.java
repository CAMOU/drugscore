package solutions.misi.drugscore.boosters;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.util.ArrayList;
import java.util.List;

public class GlobalDrugBooster extends ItemStack implements Listener {

    @Getter final String displayName;

    public GlobalDrugBooster() {
        final Material material = Material.END_CRYSTAL;
        displayName = "§dGlobal Drug Booster";
        final List<String> lore = new ArrayList<>();
        lore.add("§b§oApplies a drug selling boost of §lx2");
        lore.add("§b§oto all Drugs for 30m!");

        this.setType(material);
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(displayName);
        itemMeta.setLore(lore);
        this.setItemMeta(itemMeta);
    }

    public static void activate(Player player, ItemStack booster) {
        if(DrugsCore.getInstance().getShopBoosterHandler().isActiveGlobalBooster()) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getBoosterAlreadyActive());
            return;
        }

        DrugsCore.getInstance().getShopBoosterHandler().setActiveGlobalBooster(true);
        DrugsCore.getInstance().getShopBoosterHandler().setActiveGlobalBoosterTime(System.currentTimeMillis()+1800000);
        DrugsCore.getInstance().getSchedulers().startGlobalDrugBoosterTask();

        booster.setAmount(booster.getAmount() - 1);
        Bukkit.broadcastMessage(DrugsCore.getInstance().getMessage().getBoosterActivated().replace("[type]", "Global").replace("[drug]", "All Drugs").replace("[player]", player.getDisplayName()).replace("[time]", "30min"));
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        try {
            if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(displayName)) {
                if(event.getHand() == EquipmentSlot.HAND) {
                    DrugsCore.getInstance().getConfirmGUI().open(player, displayName);
                }
            }
        } catch(NullPointerException ex) {
        }
    }
}