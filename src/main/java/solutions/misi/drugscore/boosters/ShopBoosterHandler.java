package solutions.misi.drugscore.boosters;

import lombok.Getter;
import lombok.Setter;
import solutions.misi.drugscore.drugs.Drug;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ShopBoosterHandler {

    @Getter private final Map<Drug, Long> activeRandomBoosters = new HashMap<>();
    @Getter @Setter private boolean activeGlobalBooster = false;
    @Getter @Setter private long activeGlobalBoosterTime = 0;

    public String getFormattedTimeRemaining(long time) {
        return String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(time-System.currentTimeMillis()),
                TimeUnit.MILLISECONDS.toSeconds(time-System.currentTimeMillis()) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time-System.currentTimeMillis())));
    }
}
