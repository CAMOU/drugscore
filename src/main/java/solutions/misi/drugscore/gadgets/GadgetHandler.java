package solutions.misi.drugscore.gadgets;

import lombok.Getter;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GadgetHandler implements Listener {

    @Getter private final List<UUID> enabledAutopick = new ArrayList<>();
    @Getter private final List<UUID> enabledAutoplant = new ArrayList<>();

}
