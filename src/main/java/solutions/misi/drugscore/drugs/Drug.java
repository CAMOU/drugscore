package solutions.misi.drugscore.drugs;

import lombok.Getter;
import org.bukkit.Material;

public class Drug {

    @Getter private final String name;
    @Getter private final Material material;
    @Getter private final double price;

    public Drug(String name, Material material, double price) {
        this.name = name;
        this.material = material;
        this.price = price;
    }
}