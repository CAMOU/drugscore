package solutions.misi.drugscore.drugs.crafting;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;

public class BlankFormula extends ItemStack {

    @Getter private final double price;

    public BlankFormula() {
        File recipesFile = new File("plugins/DrugsCore/recipes.yml");
        FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);
        final Material material = Material.matchMaterial(recipesFileCfg.getString("blank-formula.material"));
        final String displayName = ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("blank-formula.name"));

        this.price = recipesFileCfg.getDouble("blank-formula.price");

        this.setType(material);
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(displayName);
        this.setItemMeta(itemMeta);

    }
}
