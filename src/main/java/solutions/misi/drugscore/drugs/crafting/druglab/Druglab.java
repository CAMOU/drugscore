package solutions.misi.drugscore.drugs.crafting.druglab;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.util.ArrayList;
import java.util.List;

public class Druglab extends ItemStack implements Listener {

    public Druglab() {
        Material material = Material.END_PORTAL_FRAME;
        String name = "§cDruglab";
        List<String> lore = new ArrayList<>();
        lore.add(" ");
        lore.add("§bPlace it down to create a druglab!");
        lore.add(" ");

        this.setType(material);
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        this.setItemMeta(itemMeta);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();

        if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals("§cDruglab")) {
            //> Create new druglab
            Location location = event.getBlockPlaced().getLocation();
            DrugsCore.getInstance().getDruglabsTable().createDruglab(location);

            player.getWorld().playEffect(location, Effect.SMOKE, 1);
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully created a druglab!");
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if(event.getBlock().getType() == Material.END_PORTAL_FRAME) {
            Player player = event.getPlayer();
            Location location = event.getBlock().getLocation();

            if(DrugsCore.getInstance().getDruglabsTable().druglabExists(location)) {
                //> Delete druglab
                DrugsCore.getInstance().getDruglabsTable().deleteDruglab(location);

                Hologram druglabHologram = null;
                for(Hologram hologram : HologramsAPI.getHolograms(DrugsCore.getInstance())) {
                    if(hologram.getX() == location.getX()+0.5 && hologram.getZ() == location.getZ()+0.5 && hologram.getY() == location.getY()+2)
                        druglabHologram = hologram;
                }

                if(druglabHologram != null)
                    druglabHologram.delete();

                event.setDropItems(false);

                Druglab druglab = new Druglab();
                druglab.setAmount(1);

                player.getWorld().dropItem(location, druglab);
                player.getWorld().playEffect(location, Effect.FENCE_GATE_CLOSE, 1);
                player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aDruglab has been deleted!");
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if(event.getHand() == EquipmentSlot.HAND) {
                if(event.getClickedBlock().getType() == Material.END_PORTAL_FRAME) {
                    Player player = event.getPlayer();
                    Location location = event.getClickedBlock().getLocation();

                    if(DrugsCore.getInstance().getDruglabsTable().druglabExists(location)) {
                        //> Open druglab
                        DrugsCore.getInstance().getDrugslabGUI().open(player, location);
                    }
                }
            }
        }
    }
}