package solutions.misi.drugscore.drugs.crafting;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipesManager {

    public Map<Integer, ItemStack> getFormulaShopItems() {
        File recipesFile = new File("plugins/DrugsCore/recipes.yml");
        FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);
        Map<Integer, ItemStack> formulaShopItems = new HashMap<>();

        for(String path : recipesFileCfg.getConfigurationSection("recipes").getKeys(false)) {
            Material material = Material.matchMaterial(recipesFileCfg.getString("recipes." + path + ".item.material"));
            int amount = recipesFileCfg.getInt("recipes." + path + ".item.amount");
            String name = ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("recipes." + path + ".item.name"));
            List<String> lore = new ArrayList<>();
            for(String loreElement : recipesFileCfg.getStringList("recipes." + path + ".item.lore"))
                lore.add(ChatColor.translateAlternateColorCodes('&', loreElement));

            ItemStack itemStack = new ItemStack(material, amount);
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(name);
            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);

            formulaShopItems.put(formulaShopItems.size(), itemStack);
        }

        return formulaShopItems;
    }

    public List<ItemStack> getPurchasedRecipes(Player player) {
        File recipesFile = new File("plugins/DrugsCore/recipes.yml");
        FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);
        List<ItemStack> purchasedRecipes = new ArrayList<>();

        for(String path : recipesFileCfg.getConfigurationSection("recipes").getKeys(false)) {
            String recipePermission = recipesFileCfg.getString("recipes." + path + ".permission");

            if(player.hasPermission(recipePermission)) {
                Material material = Material.matchMaterial(recipesFileCfg.getString("recipes." + path + ".item.material"));
                int amount = recipesFileCfg.getInt("recipes." + path + ".item.amount");
                String name = ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("recipes." + path + ".item.name"));
                List<String> lore = new ArrayList<>();
                for(String loreElement : recipesFileCfg.getStringList("recipes." + path + ".item.lore"))
                    lore.add(ChatColor.translateAlternateColorCodes('&', loreElement));

                ItemStack itemStack = new ItemStack(material, amount);
                ItemMeta itemMeta = itemStack.getItemMeta();
                itemMeta.setDisplayName(name);
                itemMeta.setLore(lore);
                itemStack.setItemMeta(itemMeta);

                purchasedRecipes.add(itemStack);
            }
        }

        return purchasedRecipes;
    }

    public String getPermission(String name) {
        String permission = "";
        File recipesFile = new File("plugins/DrugsCore/recipes.yml");
        FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);

        for(String path : recipesFileCfg.getConfigurationSection("recipes").getKeys(false)) {
            if(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("recipes." + path + ".item.name"))).equals(name)) {
                permission = recipesFileCfg.getString("recipes." + path + ".permission");
            }
        }

        return permission;
    }

    public Map<Integer, ItemStack> getDruglabItems(Player player) {
        File drugslabFile = new File("plugins/DrugsCore/drugslab.yml");
        FileConfiguration drugslabFileCfg = YamlConfiguration.loadConfiguration(drugslabFile);
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);
        Map<Integer, ItemStack> drugslabItems = new HashMap<>();
        String playerDrugLevelName = ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId()) + ".name"));


        for(String path : drugslabFileCfg.getConfigurationSection("recipes").getKeys(false)) {
            Material material = Material.matchMaterial(drugslabFileCfg.getString("recipes." + path + ".result.material"));
            //int amount = drugslabFileCfg.getInt("recipes." + path + ".result.amount");
            int amount = 1;
            String name = ChatColor.translateAlternateColorCodes('&', drugslabFileCfg.getString("recipes." + path + ".result.name"));
            String permission = drugslabFileCfg.getString("recipes." + path + ".permission");
            List<String> lore = new ArrayList<>();
            lore.add(" ");

            //> Set lore no permission
            if(!player.hasPermission(permission)) {
                lore.add("    §c§lNOT UNLOCKED    ");
            } else {
                boolean playerHasResources = true;
                for(ItemStack craftingItem : getDrugslabCraftingItems(path)) {
                    ItemStack craftingItemAlternative = craftingItem.clone();
                    ItemMeta craftingItemAlternativeMeta = craftingItemAlternative.getItemMeta();
                    List<String> craftingItemAlternativeLore = new ArrayList<>();
                    craftingItemAlternativeLore.add(" ");
                    craftingItemAlternativeLore.add("§7Drug Level: " + playerDrugLevelName);
                    craftingItemAlternativeLore.add(" ");
                    craftingItemAlternativeMeta.setLore(craftingItemAlternativeLore);
                    craftingItemAlternative.setItemMeta(craftingItemAlternativeMeta);

                    if(!(player.getInventory().containsAtLeast(craftingItem, craftingItem.getAmount()) || player.getInventory().containsAtLeast(craftingItemAlternative, craftingItemAlternative.getAmount()))) {
                        playerHasResources = false;
                        break;
                    }
                }

                if(!playerHasResources) {
                    //> Set lore permission + no resources
                    lore.add("    §c§lNO RESOURCES    ");
                } else {
                    //> Set lore permission + resources
                    lore.add("    §a§lCRAFTABLE    ");
                }
            }

            lore.add(" ");
            for(String customLore : drugslabFileCfg.getStringList("recipes." + path + ".result.lore"))
                lore.add(ChatColor.translateAlternateColorCodes('&', customLore));

            ItemStack itemStack = new ItemStack(material, amount);
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(name);
            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);

            drugslabItems.put(drugslabItems.size(), itemStack);
        }

        return drugslabItems;
    }

    public List<ItemStack> getDrugslabCraftingItems(String path) {
        File drugslabFile = new File("plugins/DrugsCore/drugslab.yml");
        FileConfiguration drugslabFileCfg = YamlConfiguration.loadConfiguration(drugslabFile);
        List<ItemStack> drugslabCraftingItems = new ArrayList<>();

        for(String item : drugslabFileCfg.getConfigurationSection("recipes." + path + ".crafting").getKeys(false)) {
            Material material = Material.matchMaterial(drugslabFileCfg.getString("recipes." + path + ".crafting." + item + ".material"));
            int amount = drugslabFileCfg.getInt("recipes." + path + ".crafting." + item + ".amount");
            String name = ChatColor.translateAlternateColorCodes('&', drugslabFileCfg.getString("recipes." + path + ".crafting." + item + ".name"));
            List<String> lore = new ArrayList<>();
            for(String loreElement : drugslabFileCfg.getStringList("recipes." + path + ".crafting." + item + ".lore"))
                lore.add(ChatColor.translateAlternateColorCodes('&', loreElement));

            ItemStack craftingItem = new ItemStack(material, amount);
            ItemMeta craftingItemMeta = craftingItem.getItemMeta();
            craftingItemMeta.setDisplayName(name);
            craftingItemMeta.setLore(lore);
            craftingItem.setItemMeta(craftingItemMeta);

            drugslabCraftingItems.add(craftingItem);
        }

        return drugslabCraftingItems;
    }

    public String findDruglabCraftingItemByName(String name) {
        File drugslabFile = new File("plugins/DrugsCore/drugslab.yml");
        FileConfiguration drugslabFileCfg = YamlConfiguration.loadConfiguration(drugslabFile);

        for(String path : drugslabFileCfg.getConfigurationSection("recipes").getKeys(false)) {
            String resultName = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', drugslabFileCfg.getString("recipes." + path + ".result.name")));

            if(ChatColor.stripColor(name).equals(ChatColor.stripColor(resultName)))
                return path;
        }

        return null;
    }
}