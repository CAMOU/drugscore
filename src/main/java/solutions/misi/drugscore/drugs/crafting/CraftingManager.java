package solutions.misi.drugscore.drugs.crafting;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CraftingManager {

    public void loadCraftingRecipes() {
        File craftingFile = new File("plugins/DrugsCore/crafting.yml");
        FileConfiguration craftingFileCfg = YamlConfiguration.loadConfiguration(craftingFile);

        //> Workbench recipes
        for(String path : craftingFileCfg.getConfigurationSection("crafting").getKeys(false)) {
            ItemStack result = getCraftingItem(craftingFileCfg.getString("crafting." + path + ".result"));
            NamespacedKey key = new NamespacedKey(DrugsCore.getInstance(), path);
            ShapedRecipe recipe = new ShapedRecipe(key, result);
            String[] firstRow = craftingFileCfg.getStringList("crafting." + path + ".shape").get(0).split(",");
            String[] secondRow = craftingFileCfg.getStringList("crafting." + path + ".shape").get(1).split(",");
            String[] thirdRow = craftingFileCfg.getStringList("crafting." + path + ".shape").get(2).split(",");

            recipe.shape("ABC", "DEF", "GHI");

            RecipeChoice A = new RecipeChoice.ExactChoice(getCraftingItem(firstRow[0]));
            recipe.setIngredient('A', A);
            RecipeChoice B = new RecipeChoice.ExactChoice(getCraftingItem(firstRow[1]));
            recipe.setIngredient('B', B);
            RecipeChoice C = new RecipeChoice.ExactChoice(getCraftingItem(firstRow[2]));
            recipe.setIngredient('C', C);

            RecipeChoice D = new RecipeChoice.ExactChoice(getCraftingItem(secondRow[0]));
            recipe.setIngredient('D', D);
            RecipeChoice E = new RecipeChoice.ExactChoice(getCraftingItem(secondRow[1]));
            recipe.setIngredient('E', E);
            RecipeChoice F = new RecipeChoice.ExactChoice(getCraftingItem(secondRow[2]));
            recipe.setIngredient('F', F);

            RecipeChoice G = new RecipeChoice.ExactChoice(getCraftingItem(thirdRow[0]));
            recipe.setIngredient('G', G);
            RecipeChoice H = new RecipeChoice.ExactChoice(getCraftingItem(thirdRow[1]));
            recipe.setIngredient('H', H);
            RecipeChoice I = new RecipeChoice.ExactChoice(getCraftingItem(thirdRow[2]));
            recipe.setIngredient('I', I);

            try {
                Bukkit.addRecipe(recipe);
            } catch(IllegalStateException ex) {}
        }

        //> Furnace recipes
        for(String path : craftingFileCfg.getConfigurationSection("furnace").getKeys(false)) {
            ItemStack item = getCraftingItem(craftingFileCfg.getString("furnace." + path + ".item"));
            ItemStack result = getCraftingItem(craftingFileCfg.getString("furnace." + path + ".result"));
            NamespacedKey key = new NamespacedKey(DrugsCore.getInstance(), path);
            RecipeChoice recipeChoice = new RecipeChoice.ExactChoice(item);

            try {
                Bukkit.addRecipe(new FurnaceRecipe(key, result, recipeChoice, 0, 200));
            } catch(IllegalStateException ex) {}
        }
    }

    private ItemStack getCraftingItem(String name) {
        File craftingFile = new File("plugins/DrugsCore/crafting.yml");
        FileConfiguration craftingFileCfg = YamlConfiguration.loadConfiguration(craftingFile);

        String displayName = craftingFileCfg.getString("items." + name + ".name");
        Material material = Material.matchMaterial(craftingFileCfg.getString("items." + name + ".material"));
        ItemStack craftingItem = new ItemStack(material);
        ItemMeta craftingItemMeta = craftingItem.getItemMeta();
        try {
            craftingItemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
            List<String> craftingItemLore = new ArrayList<>();
            for(String loreElement : craftingFileCfg.getStringList("items." + name + ".lore"))
                craftingItemLore.add(ChatColor.translateAlternateColorCodes('&', loreElement));
            craftingItemMeta.setLore(craftingItemLore);
        } catch(NullPointerException ex) {}
        craftingItem.setItemMeta(craftingItemMeta);

        return craftingItem;
    }
}
