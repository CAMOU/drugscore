package solutions.misi.drugscore.drugs.crafting.druglab;

import lombok.Getter;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DruglabManager {

    @Getter private final Map<UUID, Location> openedDruglabs;

    public DruglabManager() {
        openedDruglabs = new HashMap<>();
    }
}
