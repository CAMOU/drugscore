package solutions.misi.drugscore.drugs.crafting;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;

public class Recipe extends ItemStack implements Listener {

    public Recipe() {
        this.setType(Material.ENCHANTED_BOOK);
    }

    public void create(String displayName, int amount) {
        ItemMeta itemMeta = this.getItemMeta();
        itemMeta.setDisplayName(displayName);
        this.setItemMeta(itemMeta);
        this.setAmount(amount);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        try {
            File recipesFile = new File("plugins/DrugsCore/recipes.yml");
            FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);

            String recipeName = "";
            for(String path : recipesFileCfg.getConfigurationSection("recipes").getKeys(false)) {
                if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName()
                        .equals(ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("recipes." + path + ".item.name")))) {
                    recipeName = ChatColor.translateAlternateColorCodes('&', recipesFileCfg.getString("recipes." + path + ".item.name"));
                    break;
                }
            }

            if(recipeName == "")
                return;

            DrugsCore.getInstance().getConfirmGUI().open(player, recipeName);
        } catch(NullPointerException ex) {
        }
    }
}
