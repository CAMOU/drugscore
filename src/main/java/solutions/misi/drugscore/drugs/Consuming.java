package solutions.misi.drugscore.drugs;

import lombok.Getter;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Consuming implements Listener {

    @Getter private final List<UUID> activeConsuming = new ArrayList<>();

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        File drugsFile = new File("plugins/DrugsCore/drugs.yml");
        FileConfiguration drugsFileCfg = YamlConfiguration.loadConfiguration(drugsFile);

        if(player.isSneaking()) {
            if(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                if(event.getHand() == EquipmentSlot.HAND) {
                    try {
                        if(player.getInventory().getItemInMainHand().getType() != Material.AIR) {
                            List<String> rawConsumeEffects = null;
                            boolean randomDisguise = false;
                            int duration = 0;
                            int playerDeathChance = 0;

                            for(String path : drugsFileCfg.getKeys(false)) {
                                if(player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', drugsFileCfg.getString(path + ".name")))) {
                                    rawConsumeEffects = drugsFileCfg.getStringList(path + ".consume.effects");
                                    randomDisguise = drugsFileCfg.getBoolean(path + ".consume.random-disguise");
                                    duration = drugsFileCfg.getInt(path + ".consume.duration");
                                    playerDeathChance = drugsFileCfg.getInt(path + ".consume.player-death-chance");
                                    break;
                                }
                            }

                            if(rawConsumeEffects == null) { return; }
                            if(!DrugsCore.getInstance().getConsuming().getActiveConsuming().contains(player.getUniqueId())) {
                                DrugsCore.getInstance().getConsuming().getActiveConsuming().add(player.getUniqueId());
                                DrugsCore.getInstance().getSchedulers().removeDisguiseAfterDuration(player, duration);
                            }

                            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_BURP, 1.0F, 1.0F);
                            player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);

                            //> Player Death Chance
                            Random deathChance = new Random();
                            int deathChancePercentage = deathChance.nextInt(100);
                            if(playerDeathChance >= deathChancePercentage) player.damage(10000.0);

                            //> Potion Effects
                            for(String rawEffect : rawConsumeEffects) {
                                String[] effect = rawEffect.split(":");
                                int amplifier = Integer.parseInt(effect[1])-1;
                                PotionEffect potionEffect = new PotionEffect(PotionEffectType.getByName(effect[0]), 20*duration, amplifier);

                                player.addPotionEffect(potionEffect);
                            }

                            //> Random Disguise
                            if(randomDisguise) {
                                File disguisesFile = new File("plugins/DrugsCore/disguises.yml");
                                FileConfiguration disguisesFileCfg = YamlConfiguration.loadConfiguration(disguisesFile);

                                List<String> possibleDisguises = disguisesFileCfg.getStringList("disguises");
                                Random randomDisguiseChance = new Random();
                                int randomDisguiseIndex = randomDisguiseChance.nextInt(possibleDisguises.size());

                                EntityType entityType = EntityType.fromName(possibleDisguises.get(randomDisguiseIndex));
                                MobDisguise disguise = new MobDisguise(DisguiseType.getType(entityType));
                                disguise.setEntity(player);
                                disguise.startDisguise();
                            }
                        }
                    } catch(NullPointerException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
}