package solutions.misi.drugscore.drugs;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugsManager {

    //> Player Boost Permissions:
    //>     drugscore.playerboost.1-15

    @Getter @Setter public List<Drug> drugs;
    @Getter @Setter public List<String> drugNames;

    public DrugsManager() {
        drugs = new ArrayList<>();
        drugNames = new ArrayList<>();
    }

    public void loadDrugsFromFile() {
        File drugsFile = new File(DrugsCore.getInstance().getDataFolder(), "drugs.yml");
        FileConfiguration drugsFileCfg = YamlConfiguration.loadConfiguration(drugsFile);
        List<Drug> drugs = new ArrayList<>();
        List<String> drugNames = new ArrayList<>();

        for(String path : drugsFileCfg.getKeys(false)) {
            String drugName = ChatColor.translateAlternateColorCodes('&', drugsFileCfg.getString(path + ".name"));
            Material drugMaterial = Material.getMaterial(drugsFileCfg.getString(path + ".material"));
            double drugPrice = drugsFileCfg.getDouble(path + ".price");

            Drug drug = new Drug(drugName, drugMaterial, drugPrice);
            drugs.add(drug);
            drugNames.add(ChatColor.stripColor(drugName));
        }

        setDrugs(drugs);
        setDrugNames(drugNames);
    }

    public void sellDrugs(ItemStack[] soldItems, Player player) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);

        double sellingPrice = 0.0;
        int sold = 0;

        for(ItemStack soldItem : soldItems) {
            if(soldItem == null)
                continue;

            Drug drug = findDrugByName(soldItem.getItemMeta().getDisplayName());

            if(drug == null) {
                DrugsCore.getInstance().getItemHandler().giveItemToPlayer(player, soldItem);
                continue;
            }

            double drugPrice = drug.getPrice();

            int playerDrugLevel = DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId());
            double playerDrugLevelBoost = drugexpFileCfg.getDouble("levels." + playerDrugLevel + ".sell-boost");
            drugPrice = drugPrice * playerDrugLevelBoost;

            double playerBooster = getPlayerBoost(player);

            playerBooster = drugPrice / 100 * playerBooster;
            drugPrice += playerBooster;

            //> Boosters
            if(DrugsCore.getInstance().getShopBoosterHandler().isActiveGlobalBooster())
                drugPrice += drugPrice;
            if(DrugsCore.getInstance().getShopBoosterHandler().getActiveRandomBoosters().containsKey(drug))
                drugPrice += drugPrice;

            //> Final selling price
            sellingPrice += drugPrice * soldItem.getAmount();
            sold += soldItem.getAmount();
        }

        sellingPrice = Math.round(sellingPrice * 100.0) / 100.0;

        if(sold == 0)
            return;

        DrugsCore.getInstance().getEconomy().depositPlayer(player, sellingPrice);
        player.sendMessage(DrugsCore.getInstance().getMessage().getSoldDrugs().replace("[amount]", sold + "").replace("[price]", "" + sellingPrice));
    }

    public Drug findDrugByName(String name) {
        for(Drug drug : drugs)
            if(ChatColor.stripColor(name).equalsIgnoreCase(ChatColor.stripColor(drug.getName()))) return drug;
        return null;
    }

    public Drug findDrugByMaterial(Material material) {
        for(Drug drug : drugs)
            if(material == drug.getMaterial()) return drug;
        return null;
    }

    public int getPlayerBoost(Player player) {
        int playerBooster = 0;

        for(int i = 1; i <= 15; i++) {
            if(player.hasPermission("drugscore.playerboost." + i))
                playerBooster = i;
        }

        return playerBooster;
    }
}
