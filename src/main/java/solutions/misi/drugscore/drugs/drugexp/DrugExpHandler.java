package solutions.misi.drugscore.drugs.drugexp;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.*;

public class DrugExpHandler implements Listener {

    @Getter public Map<UUID, Integer> drugExpLevelCache;
    @Getter public Map<UUID, Double> drugExpCache;

    public DrugExpHandler() {
        drugExpLevelCache = new HashMap<>();
        drugExpCache = new HashMap<>();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        DrugsCore.getInstance().getDrugExpTable().registerPlayer(player);

        if(!getDrugExpLevelCache().containsKey(player.getUniqueId()))
            DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().put(player.getUniqueId(), DrugsCore.getInstance().getDrugExpTable().getPlayerExpLevel(player));

        if(!getDrugExpCache().containsKey(player.getUniqueId()))
            DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().put(player.getUniqueId(), DrugsCore.getInstance().getDrugExpTable().getPlayerExp(player));
    }

    public void saveCacheData() {
        for(Map.Entry entry : drugExpLevelCache.entrySet())
            DrugsCore.getInstance().getDrugExpTable().setPlayerExpLevel((UUID) entry.getKey(), (int) entry.getValue());
        for(Map.Entry entry : drugExpCache.entrySet())
            DrugsCore.getInstance().getDrugExpTable().setPlayerExp((UUID) entry.getKey(), (double) entry.getValue());
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);
        Player player = event.getPlayer();
        String playerDrugLevelName = ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId()) + ".name"));

        if(event.getBlock().getType() == Material.SUGAR_CANE ||
                event.getBlock().getType() == Material.CARROTS ||
                event.getBlock().getType() == Material.POTATOES ||
                event.getBlock().getType() == Material.PUMPKIN ||
                event.getBlock().getType() == Material.MELON ||
                event.getBlock().getType() == Material.NETHER_WART ||
                event.getBlock().getType() == Material.COCOA_BEANS ||
                event.getBlock().getType() == Material.RED_MUSHROOM ||
                event.getBlock().getType() == Material.BROWN_MUSHROOM ||
                event.getBlock().getType() == Material.WHEAT ||
                event.getBlock().getType() == Material.CHORUS_FRUIT ||
                event.getBlock().getType() == Material.BEETROOT ||
                event.getBlock().getType() == Material.SWEET_BERRIES ||
                event.getBlock().getType() == Material.CACTUS) {

            boolean givePlayerExp = false;
            for(ItemStack drop : event.getBlock().getDrops()) {
                event.setDropItems(false);

                ItemMeta dropMeta = drop.getItemMeta();
                try {
                    String drugName = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(drop.getType()).getName();
                    if(drugName != null) {
                        dropMeta.setDisplayName(drugName);
                        List<String> dropLore = new ArrayList<>();
                        dropLore.add(" ");
                        dropLore.add("§7Drug Level: " + playerDrugLevelName);
                        dropLore.add(" ");
                        dropMeta.setLore(dropLore);
                        drop.setItemMeta(dropMeta);
                        givePlayerExp = true;
                    }
                } catch(NullPointerException ex) {}

                //> Autopickup
                if(DrugsCore.getInstance().getGadgetHandler().getEnabledAutopick().contains(player.getUniqueId())) {
                    HashMap<Integer, ItemStack> leftItems = player.getInventory().addItem(drop);

                    if(leftItems.isEmpty())
                        continue;
                }

                event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
            }

            //> Give player exp
            if(givePlayerExp) {
                double drugExp = findDrugExpByMaterial(event.getBlock().getType());
                addPlayerDrugExp(player, drugExp);
            }

            //> Autoplant
            if(DrugsCore.getInstance().getGadgetHandler().getEnabledAutoplant().contains(player.getUniqueId())) {
                Material cropMaterial;
                Material plantingMaterial;

                switch(event.getBlock().getType()) {
                    case WHEAT:
                        plantingMaterial = Material.WHEAT_SEEDS;
                        cropMaterial = Material.WHEAT;
                        break;
                    case BEETROOT:
                        plantingMaterial = Material.BEETROOT_SEEDS;
                        cropMaterial = Material.BEETROOT;
                        break;
                    default:
                        plantingMaterial = event.getBlock().getType();
                        cropMaterial = plantingMaterial;
                        break;
                }

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        for(int i = 0; i < player.getInventory().getSize(); i++) {
                            try {
                                if(player.getInventory().getItem(i).getType() == plantingMaterial) {
                                    player.getInventory().getItem(i).setAmount(player.getInventory().getItem(i).getAmount() - 1);
                                    event.getBlock().setType(cropMaterial);
                                    break;
                                }
                            } catch(NullPointerException ex) {}
                        }
                    }
                }.runTaskLater(DrugsCore.getInstance(), 2);
            }
        }
    }

    @EventHandler
    public void onLiquidDestroyCrops(BlockFromToEvent event) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);

        for(ItemStack drop : event.getToBlock().getDrops()) {
            if(drop.getType() == Material.SUGAR_CANE ||
                    drop.getType() == Material.CARROTS ||
                    drop.getType() == Material.POTATOES ||
                    drop.getType() == Material.PUMPKIN ||
                    drop.getType() == Material.MELON ||
                    drop.getType() == Material.NETHER_WART ||
                    drop.getType() == Material.COCOA_BEANS ||
                    drop.getType() == Material.RED_MUSHROOM ||
                    drop.getType() == Material.BROWN_MUSHROOM ||
                    drop.getType() == Material.WHEAT ||
                    drop.getType() == Material.CHORUS_FRUIT ||
                    drop.getType() == Material.BEETROOT ||
                    drop.getType() == Material.SWEET_BERRIES ||
                    drop.getType() == Material.CACTUS) {

                ItemMeta dropMeta = drop.getItemMeta();
                dropMeta.setDisplayName(DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(drop.getType()).getName());
                List<String> dropLore = new ArrayList<>();
                dropLore.add(" ");
                dropLore.add("§7Drug Level: " + ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels.11.name")));
                dropLore.add(" ");
                dropMeta.setLore(dropLore);
                drop.setItemMeta(dropMeta);
            }

            event.setCancelled(true);
            event.getToBlock().setType(Material.AIR);
            event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
        }
    }

    @EventHandler
    public void onPistonDestroyCrops(BlockPistonExtendEvent event) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);

        for(Block block : event.getBlocks()) {
            for(ItemStack drop : block.getDrops()) {
                if(drop.getType() == Material.SUGAR_CANE ||
                        drop.getType() == Material.CARROTS ||
                        drop.getType() == Material.POTATOES ||
                        drop.getType() == Material.PUMPKIN ||
                        drop.getType() == Material.MELON ||
                        drop.getType() == Material.NETHER_WART ||
                        drop.getType() == Material.COCOA_BEANS ||
                        drop.getType() == Material.RED_MUSHROOM ||
                        drop.getType() == Material.BROWN_MUSHROOM ||
                        drop.getType() == Material.WHEAT ||
                        drop.getType() == Material.CHORUS_FRUIT ||
                        drop.getType() == Material.BEETROOT ||
                        drop.getType() == Material.SWEET_BERRIES ||
                        drop.getType() == Material.CACTUS) {

                    ItemMeta dropMeta = drop.getItemMeta();
                    dropMeta.setDisplayName(DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(drop.getType()).getName());
                    List<String> dropLore = new ArrayList<>();
                    dropLore.add(" ");
                    dropLore.add("§7Drug Level: " + ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels.11.name")));
                    dropLore.add(" ");
                    dropMeta.setLore(dropLore);
                    drop.setItemMeta(dropMeta);

                    event.setCancelled(true);
                    block.setType(Material.AIR);
                    event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), drop);
                }
            }
        }
    }

    public double getNextLevelExpNeeded(Player player) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);

        int nextDrugExpLevel = drugExpLevelCache.get(player.getUniqueId())+1;
        if(nextDrugExpLevel == 11) return 0.0;
        double nextLevelExpNeeded = drugexpFileCfg.getDouble("levels." + nextDrugExpLevel + ".xp-needed");

        return nextLevelExpNeeded;
    }

    private double findDrugExpByMaterial(Material material) {
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);

        for(String path : drugexpFileCfg.getConfigurationSection("crops").getKeys(false)) {
            if(material.name().equals(path.toUpperCase())) {
                return drugexpFileCfg.getDouble("crops." + path + ".xp-worth");
            }
        }

        return 0.0;
    }

    private void addPlayerDrugExp(Player player, double exp) {
        double currentPlayerDrugExp = DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().get(player.getUniqueId());

        DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().put(player.getUniqueId(), currentPlayerDrugExp + exp);

        int currentPlayerDrugLevel = DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId());
        double xpUntilNextLevel = DrugsCore.getInstance().getDrugExpHandler().getNextLevelExpNeeded(player) - currentPlayerDrugExp;
        if(xpUntilNextLevel <= 0) {
            if(currentPlayerDrugLevel == 10) return;
            DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().put(player.getUniqueId(), currentPlayerDrugLevel+1);
            DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().put(player.getUniqueId(), 0.0);

            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aYou have reached DrugExp level §6" + (currentPlayerDrugLevel+1) + "§a!");
            player.playSound(player.getLocation(), Sound.ENTITY_VINDICATOR_CELEBRATE, 1.0F, 1.0F);
        }
    }
}
