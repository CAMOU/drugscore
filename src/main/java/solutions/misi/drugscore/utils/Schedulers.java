package solutions.misi.drugscore.utils;

import lombok.Getter;
import me.libraryaddict.disguise.DisguiseAPI;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import solutions.misi.drugscore.drugs.Drug;
import solutions.misi.drugscore.plugin.DrugsCore;

public class Schedulers {

    @Getter public BukkitTask autosave;

    /******(  BOOSTER SCHEDULERS  )******/
    public void startRandomDrugBoosterTask(Drug drug) {
        //> Remove drug booster after 15 minutes
        new BukkitRunnable() {
            @Override
            public void run() {
                DrugsCore.getInstance().getShopBoosterHandler().getActiveRandomBoosters().remove(drug);
            }
        }.runTaskLaterAsynchronously(DrugsCore.getInstance(), 20*60*15);
    }

    public void startGlobalDrugBoosterTask() {
        //> Remove drug booster after 30 minutes
        new BukkitRunnable() {
            @Override
            public void run() {
                DrugsCore.getInstance().getShopBoosterHandler().setActiveGlobalBooster(false);
            }
        }.runTaskLaterAsynchronously(DrugsCore.getInstance(), 20*60*30);
    }

    /******(  DRUGEXP SCHEDULERS  )******/
    public void startAutosave() {
        autosave = new BukkitRunnable() {
            @Override
            public void run() {
                if(DrugsCore.getInstance() != null) {
                    DrugsCore.getInstance().getDrugExpHandler().saveCacheData();
                    System.out.println("[DrugsCore] Auto-Saved user data..");
                } else {
                    cancel();
                }
            }
        }.runTaskTimerAsynchronously(DrugsCore.getInstance(), 20*60*8, 20*60*8);
    }

    /******(  DRUG CONSUME SCHEDULERS  )******/
    public void removeDisguiseAfterDuration(Player player, int duration) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    DisguiseAPI.getDisguise(player).removeDisguise();
                } catch(NullPointerException ex) {}

                DrugsCore.getInstance().getConsuming().getActiveConsuming().remove(player.getUniqueId());
            }
        }.runTaskLater(DrugsCore.getInstance(), 20*duration);
    }
}
