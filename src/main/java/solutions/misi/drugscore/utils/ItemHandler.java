package solutions.misi.drugscore.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import solutions.misi.drugscore.plugin.DrugsCore;

public class ItemHandler {

    public void giveItemToPlayer(Player player, ItemStack itemStack) {
        if(player.getInventory().firstEmpty() == -1) {
            //> Inventory is full
            player.getWorld().dropItem(player.getLocation(), itemStack);
            player.sendMessage(DrugsCore.getInstance().getMessage().getFullInventory());
        } else {
            player.getInventory().addItem(itemStack);
        }
    }
}
