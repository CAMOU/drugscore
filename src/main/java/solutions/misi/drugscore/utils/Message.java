package solutions.misi.drugscore.utils;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Message {

    @Getter private final String prefix;
    @Getter private final String wrongCommandUsage;
    @Getter private final String wrongNumberUsage;
    @Getter private final String noPermission;
    @Getter private final String drugDoesntExist;
    @Getter private final String changedDrugprice;
    @Getter private final String soldDrugs;
    @Getter private final String playerNotOnline;
    @Getter private final String boosterActivated;
    @Getter private final String boosterAlreadyActive;
    @Getter private final String notEnoughMoney;
    @Getter private final String fullInventory;

    public Message() {
        File messagesFile = new File("plugins/DrugsCore/messages.yml");
        FileConfiguration messagesFileCfg = YamlConfiguration.loadConfiguration(messagesFile);

        prefix = ChatColor.translateAlternateColorCodes('&', messagesFileCfg.getString("prefix"));
        wrongCommandUsage = getFormattedMessage("wrong-command-usage");
        wrongNumberUsage = getFormattedMessage("wrong-number-usage");
        noPermission = getFormattedMessage("no-permission");
        drugDoesntExist = getFormattedMessage("drug-doesnt-exist");
        changedDrugprice = getFormattedMessage("changed-drugprice");
        soldDrugs = getFormattedMessage("sold-drugs");
        playerNotOnline = getFormattedMessage("player-not-online");
        boosterActivated = getFormattedMessage("booster-activated");
        boosterAlreadyActive = getFormattedMessage("booster-already-active");
        notEnoughMoney = getFormattedMessage("not-enough-money");
        fullInventory = getFormattedMessage("inventory-full");
    }

    private String getFormattedMessage(String message) {
        File messagesFile = new File("plugins/DrugsCore/messages.yml");
        FileConfiguration messagesFileCfg = YamlConfiguration.loadConfiguration(messagesFile);

        return prefix + ChatColor.translateAlternateColorCodes('&', messagesFileCfg.getString(message));
    }
}
