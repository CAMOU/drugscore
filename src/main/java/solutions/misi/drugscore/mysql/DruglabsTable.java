package solutions.misi.drugscore.mysql;

import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.sql.*;

public class DruglabsTable {

    public void createTable() {
        try(Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
            Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS druglabs (world VARCHAR(255), x INT, y INT, z INT)");
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }
    }

    public void createDruglab(Location location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
                    PreparedStatement insert = connection.prepareStatement("INSERT INTO druglabs (world, x, y, z) VALUES (?, ?, ?, ?)")) {
                    insert.setString(1, location.getWorld().getName());
                    insert.setInt(2, location.getBlockX());
                    insert.setInt(3, location.getBlockY());
                    insert.setInt(4, location.getBlockZ());
                    insert.execute();
                } catch(SQLException ex) {
                    DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
                }
            }
        }.runTaskAsynchronously(DrugsCore.getInstance());
    }

    public void deleteDruglab(Location location) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
                    PreparedStatement delete = connection.prepareStatement("DELETE FROM druglabs WHERE world=? AND x=? AND y=? AND z=?")) {
                    delete.setString(1, location.getWorld().getName());
                    delete.setInt(2, location.getBlockX());
                    delete.setInt(3, location.getBlockY());
                    delete.setInt(4, location.getBlockZ());
                    delete.executeUpdate();
                } catch(SQLException ex) {
                    DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
                }
            }
        }.runTaskAsynchronously(DrugsCore.getInstance());
    }

    public boolean druglabExists(Location location) {
        boolean foundDruglab = false;

        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
            PreparedStatement select = connection.prepareStatement("SELECT 1 FROM druglabs WHERE world=? AND x=? AND y=? AND z=?")) {
            select.setString(1, location.getWorld().getName());
            select.setInt(2, location.getBlockX());
            select.setInt(3, location.getBlockY());
            select.setInt(4, location.getBlockZ());

            ResultSet results = select.executeQuery();
            if(results.next()) {
                foundDruglab = true;
            }

            results.close();
        } catch(SQLException ex) {
            foundDruglab = false;
        }

        return foundDruglab;
    }
}