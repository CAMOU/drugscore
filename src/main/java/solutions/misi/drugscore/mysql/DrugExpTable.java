package solutions.misi.drugscore.mysql;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.sql.*;
import java.util.UUID;

public class DrugExpTable {

    public void createTable() {
        try(Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
            Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS drugexp (uuid VARCHAR(36), level INT, exp FLOAT)");
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }
    }

    public void registerPlayer(Player player) {
        if(playerExists(player)) return;
        String uuid = player.getUniqueId().toString();
        new BukkitRunnable() {
            @Override
            public void run() {
                try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
                     PreparedStatement insert = connection.prepareStatement("INSERT INTO drugexp (uuid, level, exp) VALUES (?, ?, ?)")) {
                    insert.setString(1, uuid);
                    insert.setInt(2, 1);
                    insert.setDouble(3, 0);
                    insert.execute();
                } catch(SQLException ex) {
                    DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
                }
            }
        }.runTaskAsynchronously(DrugsCore.getInstance());
    }

    public double getPlayerExp(Player player) {
        String uuid = player.getUniqueId().toString();
        double playerExp = 0;
        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
             PreparedStatement select = connection.prepareStatement("SELECT exp FROM drugexp WHERE uuid=?")) {
            select.setString(1, uuid);

            ResultSet results = select.executeQuery();
            while(results.next())
                playerExp = results.getDouble("exp");
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }

        return playerExp;
    }

    public int getPlayerExpLevel(Player player) {
        String uuid = player.getUniqueId().toString();
        int playerExpLevel = 1;
        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
                PreparedStatement select = connection.prepareStatement("SELECT level FROM drugexp WHERE uuid=?")) {
            select.setString(1, uuid);

            ResultSet results = select.executeQuery();
            while(results.next())
                playerExpLevel = results.getInt("level");
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }

        return playerExpLevel;
    }

    public void setPlayerExpLevel(UUID uuid, int level) {
        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
             PreparedStatement update = connection.prepareStatement("UPDATE drugexp SET level=? WHERE uuid=?")) {
            update.setInt(1, level);
            update.setString(2, uuid.toString());
            update.executeUpdate();
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }
    }

    public void setPlayerExp(UUID uuid, double exp) {
        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
             PreparedStatement update = connection.prepareStatement("UPDATE drugexp SET exp=? WHERE uuid=?")) {
            update.setDouble(1, exp);
            update.setString(2, uuid.toString());
            update.executeUpdate();
        } catch(SQLException ex) {
            DrugsCore.getInstance().getMySQL().sendConnectionFailed(ex);
        }
    }

    private boolean playerExists(Player player) {
        String uuid = player.getUniqueId().toString();
        boolean foundPlayerExpLevel = false;

        try (Connection connection = DrugsCore.getInstance().getHikariDataSource().getConnection();
             PreparedStatement select = connection.prepareStatement("SELECT 1 FROM drugexp WHERE uuid=?")) {
            select.setString(1, uuid);

            ResultSet results = select.executeQuery();
            if(results.next()) {
                foundPlayerExpLevel = true;
            }

            results.close();
        } catch(SQLException ex) {
            foundPlayerExpLevel = false;
        }

        return foundPlayerExpLevel;
    }
}
