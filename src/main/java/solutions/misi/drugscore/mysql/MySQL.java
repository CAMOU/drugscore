package solutions.misi.drugscore.mysql;

import org.bukkit.Bukkit;
import solutions.misi.drugscore.plugin.DrugsCore;

public class MySQL {

    private void loadLoginCredentials() {
        String host = DrugsCore.getInstance().getConfig().getString("database.host");
        String port = DrugsCore.getInstance().getConfig().getString("database.port");
        String database = DrugsCore.getInstance().getConfig().getString("database.database");

        DrugsCore.getInstance().getHikariDataSource().setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database);
        DrugsCore.getInstance().getHikariDataSource().addDataSourceProperty("user", DrugsCore.getInstance().getConfig().getString("database.username"));
        DrugsCore.getInstance().getHikariDataSource().addDataSourceProperty("password", DrugsCore.getInstance().getConfig().getString("database.password"));
    }

    public void initializeDatabase() {
        loadLoginCredentials();
        DrugsCore.getInstance().getDruglabsTable().createTable();
        DrugsCore.getInstance().getDrugExpTable().createTable();
    }

    public void closeConnection() {
        if(DrugsCore.getInstance().getHikariDataSource() != null) {
            DrugsCore.getInstance().getHikariDataSource().close();
        }
    }

    public void sendConnectionFailed(Exception ex) {
        if(ex != null)
            ex.printStackTrace();

        Bukkit.getConsoleSender().sendMessage(" §4(!) §fDrugsCore couldn't start due to MySQL connection issues.");
    }
}