package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugCropsGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 18, DrugsCore.getInstance().getMessage().getPrefix() + "§2Drug Crops");

        File drugExpFile = new File(DrugsCore.getInstance().getDataFolder(), "drugexp.yml");
        FileConfiguration drugExpFileCfg = YamlConfiguration.loadConfiguration(drugExpFile);

        for(String path : drugExpFileCfg.getConfigurationSection("crops").getKeys(false)) {
            String cropExp = "§bExp for breaking: §6" + drugExpFileCfg.getDouble("crops." + path + ".xp-worth");
            Material material = Material.matchMaterial(path.toUpperCase());

            ItemStack drug = new ItemStack(material);
            ItemMeta drugMeta = drug.getItemMeta();
            List<String> drugLore = new ArrayList<>();
            drugLore.add(" ");
            drugLore.add(cropExp);
            drugLore.add(" ");
            drugMeta.setLore(drugLore);
            drug.setItemMeta(drugMeta);

            gui.addItem(drug);
        }

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§2Drug Crops"))
            event.setCancelled(true);
    }
}
