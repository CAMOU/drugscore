package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

public class PurchasedRecipesGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 45, DrugsCore.getInstance().getMessage().getPrefix() + "§9Purchased Recipes");

        ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta placeholderMeta = placeholder.getItemMeta();
        placeholderMeta.setDisplayName(" ");
        placeholder.setItemMeta(placeholderMeta);

        gui.setItem(0, placeholder);
        gui.setItem(1, placeholder);
        gui.setItem(2, placeholder);
        gui.setItem(3, placeholder);
        gui.setItem(4, placeholder);
        gui.setItem(5, placeholder);
        gui.setItem(6, placeholder);
        gui.setItem(7, placeholder);
        gui.setItem(8, placeholder);
        gui.setItem(9, placeholder);
        gui.setItem(17, placeholder);
        gui.setItem(18, placeholder);
        gui.setItem(26, placeholder);
        gui.setItem(27, placeholder);
        gui.setItem(35, placeholder);
        gui.setItem(36, placeholder);
        gui.setItem(37, placeholder);
        gui.setItem(38, placeholder);
        gui.setItem(39, placeholder);
        gui.setItem(40, placeholder);
        gui.setItem(41, placeholder);
        gui.setItem(42, placeholder);
        gui.setItem(43, placeholder);
        gui.setItem(44, placeholder);

        for(ItemStack recipe : DrugsCore.getInstance().getRecipesManager().getPurchasedRecipes(player))
            gui.addItem(recipe);

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Purchased Recipes")) {
            event.setCancelled(true);
        }
    }
}
