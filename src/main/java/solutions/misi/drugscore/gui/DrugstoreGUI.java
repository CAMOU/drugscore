package solutions.misi.drugscore.gui;

import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.drugs.Drug;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DrugstoreGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 54, DrugsCore.getInstance().getMessage().getPrefix() + "§9Sell your Drugs");

        ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta placeholderMeta = placeholder.getItemMeta();
        placeholderMeta.setDisplayName(" ");
        placeholder.setItemMeta(placeholderMeta);

        ItemStack activeShopBooster = new ItemStack(Material.GREEN_DYE);
        ItemMeta activeShopBoosterMeta = activeShopBooster.getItemMeta();
        activeShopBoosterMeta.setDisplayName("§aActive Shop Boosters");
        List<String> activeShopBoosterLore = new ArrayList<>();
        activeShopBoosterLore.add(" ");
        if(DrugsCore.getInstance().getShopBoosterHandler().isActiveGlobalBooster()) {
            String timeRemaining = DrugsCore.getInstance().getShopBoosterHandler().getFormattedTimeRemaining(DrugsCore.getInstance().getShopBoosterHandler().getActiveGlobalBoosterTime());
            activeShopBoosterLore.add("  §d• §d§lx2 §dGlobal Drug Boost §7(" + timeRemaining + ")  ");
        }
        for(Map.Entry<Drug, Long> entry : DrugsCore.getInstance().getShopBoosterHandler().getActiveRandomBoosters().entrySet()) {
            Drug randomDrugBoosted = entry.getKey();
            String timeRemaining = DrugsCore.getInstance().getShopBoosterHandler().getFormattedTimeRemaining(entry.getValue());
            activeShopBoosterLore.add("  §5• §5§lx2 §5" + randomDrugBoosted.getName() + " Boost §7(" + timeRemaining + ")  ");
        }
        if(activeShopBoosterLore.size() == 1)
            activeShopBoosterLore.add("  §c• No boosters active!  ");
        activeShopBoosterLore.add(" ");
        activeShopBoosterMeta.setLore(activeShopBoosterLore);
        activeShopBooster.setItemMeta(activeShopBoosterMeta);

        ItemStack pullDrugs = new ItemStack(Material.LEVER);
        ItemMeta pullDrugsMeta = pullDrugs.getItemMeta();
        pullDrugsMeta.setDisplayName("§6Pull drugs from inventory");
        pullDrugs.setItemMeta(pullDrugsMeta);

        ItemStack sellPriceBooster = new ItemStack(Material.BOOK);
        ItemMeta sellPriceBoosterMeta = sellPriceBooster.getItemMeta();
        sellPriceBoosterMeta.setDisplayName("§eYour sell price Booster");
        List<String> sellPriceBoosterLore = new ArrayList<>();
        int playerBoost = DrugsCore.getInstance().getDrugsManager().getPlayerBoost(player);
        sellPriceBoosterLore.add(" ");
        sellPriceBoosterLore.add("  §6• §6§l" + playerBoost + "% §6Rank Boost");
        sellPriceBoosterLore.add(" ");
        sellPriceBoosterMeta.setLore(sellPriceBoosterLore);
        sellPriceBooster.setItemMeta(sellPriceBoosterMeta);

        gui.setItem(45, placeholder);
        gui.setItem(46, activeShopBooster);
        gui.setItem(47, placeholder);
        gui.setItem(48, placeholder);
        gui.setItem(49, pullDrugs);
        gui.setItem(50, placeholder);
        gui.setItem(51, placeholder);
        gui.setItem(52, sellPriceBooster);
        gui.setItem(53, placeholder);

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Sell your Drugs")) {
            if(event.getSlot() >= 45)
                event.setCancelled(true);

            try {
                if(event.getCurrentItem().getItemMeta().getDisplayName().equals("§6Pull drugs from inventory")) {
                    for(ItemStack item : player.getInventory().getContents()) {
                        if(item != null) {
                            if(DrugsCore.getInstance().getDrugsManager().getDrugNames().contains(ChatColor.stripColor(item.getItemMeta().getDisplayName()))) {
                                //> Move item to sell gui
                                player.getInventory().remove(item);
                                event.getClickedInventory().addItem(item);
                            }
                        }
                    }

                    ItemStack sellDrugs = new ItemStack(Material.LIME_DYE);
                    ItemMeta sellDrugsMeta = sellDrugs.getItemMeta();
                    sellDrugsMeta.setDisplayName("§aSell drugs");
                    sellDrugs.setItemMeta(sellDrugsMeta);

                    event.getInventory().setItem(49, sellDrugs);
                } else if(event.getCurrentItem().getItemMeta().getDisplayName().equals("§aSell drugs")) {
                    player.closeInventory();

                    //> Sell drugs that were in the gui
                    ItemStack[] soldItems = new ItemStack[event.getInventory().getSize()];

                    for(int i = 0; i < event.getInventory().getSize(); i++) {
                        if(i < 45) {
                            soldItems[i] = event.getInventory().getContents()[i];
                        }
                    }

                    DrugsCore.getInstance().getDrugsManager().sellDrugs(soldItems, player);
                }
            } catch(NullPointerException ex) {
            }
        }
    }

    @EventHandler
    public void onNPCRightclick(NPCRightClickEvent event) {
        Player player = event.getClicker();
        NPC clickedNPC = event.getNPC();

        if(clickedNPC.getName().equals("§6Drugs Buyer")) {
            //> Open Drugstore
            DrugsCore.getInstance().getDrugstoreGUI().open(player);
        }
    }
}