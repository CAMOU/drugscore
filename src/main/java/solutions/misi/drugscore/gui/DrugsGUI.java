package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugsGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 27, DrugsCore.getInstance().getMessage().getPrefix() + "§9All Drugs");

        File drugsFile = new File(DrugsCore.getInstance().getDataFolder(), "drugs.yml");
        FileConfiguration drugsFileCfg = YamlConfiguration.loadConfiguration(drugsFile);

        for(String path : drugsFileCfg.getKeys(false)) {
            String drugName = ChatColor.translateAlternateColorCodes('&', drugsFileCfg.getString(path + ".name"));
            Material drugMaterial = Material.getMaterial(drugsFileCfg.getString(path + ".material"));
            String drugPrice = "§7Price: §6$" + drugsFileCfg.getDouble(path + ".price");

            ItemStack drug = new ItemStack(drugMaterial);
            ItemMeta drugMeta = drug.getItemMeta();
            drugMeta.setDisplayName(drugName);
            List<String> drugLore = new ArrayList<>();
            drugLore.add(" ");
            drugLore.add(drugPrice);
            drugLore.add(" ");
            drugMeta.setLore(drugLore);
            drug.setItemMeta(drugMeta);

            gui.addItem(drug);
        }

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9All Drugs")) {
            event.setCancelled(true);
        }
    }
}
