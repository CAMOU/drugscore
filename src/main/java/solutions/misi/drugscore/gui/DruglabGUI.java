package solutions.misi.drugscore.gui;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.List;
import java.util.Map;

public class DruglabGUI implements Listener {

    public void open(Player player, Location druglabLocation) {
        Inventory gui = Bukkit.createInventory(null, 54, DrugsCore.getInstance().getMessage().getPrefix() + "§9Druglab");

        ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta placeholderMeta = placeholder.getItemMeta();
        placeholderMeta.setDisplayName(" ");
        placeholder.setItemMeta(placeholderMeta);

        ItemStack hologramItem;
        ItemMeta hologramItemMeta;

        Hologram druglabHologram = null;
        for(Hologram hologram : HologramsAPI.getHolograms(DrugsCore.getInstance())) {
            if(hologram.getX() == druglabLocation.getX()+0.5 && hologram.getZ() == druglabLocation.getZ()+0.5 && hologram.getY() == druglabLocation.getY()+2)
                druglabHologram = hologram;
        }

        if(druglabHologram == null) {
            hologramItem = new ItemStack(Material.GREEN_STAINED_GLASS);
            hologramItemMeta = hologramItem.getItemMeta();
            hologramItemMeta.setDisplayName("§aActivate Hologram");
            hologramItem.setItemMeta(hologramItemMeta);
        } else {
            hologramItem = new ItemStack(Material.RED_STAINED_GLASS);
            hologramItemMeta = hologramItem.getItemMeta();
            hologramItemMeta.setDisplayName("§cDisable Hologram");
            hologramItem.setItemMeta(hologramItemMeta);
        }

        Map<Integer, ItemStack> drugs = DrugsCore.getInstance().getRecipesManager().getDruglabItems(player);

        for(int i = 0; i < gui.getSize(); i++)
            gui.setItem(i, placeholder);

        gui.setItem(49, hologramItem);

        int drugIndex = 10;
        for(int i = 0; i < drugs.size(); i++) {
            ItemStack recipe = drugs.get(i);

            if(!(drugIndex >= 10 && drugIndex <= 34))
                break;
            if(drugIndex == 17 || drugIndex == 26)
                drugIndex += 2;
            gui.setItem(drugIndex, recipe);

            drugIndex++;
        }

        DrugsCore.getInstance().getDruglabManager().getOpenedDruglabs().put(player.getUniqueId(), druglabLocation);
        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        try {
            if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Druglab")) {
                Location druglabLocation = DrugsCore.getInstance().getDruglabManager().getOpenedDruglabs().get(player.getUniqueId());

                event.setCancelled(true);

                switch(event.getCurrentItem().getItemMeta().getDisplayName()) {
                    case "§aActivate Hologram":
                        Hologram hologram = HologramsAPI.createHologram(DrugsCore.getInstance(), druglabLocation.clone().add(0.5, 2, 0.5));
                        hologram.appendTextLine("§cDruglab");

                        ItemStack hologramItem = new ItemStack(Material.RED_STAINED_GLASS);
                        ItemMeta hologramItemMeta = hologramItem.getItemMeta();
                        hologramItemMeta.setDisplayName("§cDisable Hologram");
                        hologramItem.setItemMeta(hologramItemMeta);

                        event.getInventory().setItem(49, hologramItem);
                        break;
                    case "§cDisable Hologram":
                        Hologram druglabHologram = null;
                        for(Hologram foundHologram : HologramsAPI.getHolograms(DrugsCore.getInstance())) {
                            if(foundHologram.getX() == druglabLocation.getX()+0.5 && foundHologram.getZ() == druglabLocation.getZ()+0.5 && foundHologram.getY() == druglabLocation.getY()+2)
                                druglabHologram = foundHologram;
                        }

                        if(druglabHologram != null)
                            druglabHologram.delete();

                        ItemStack activeHologramItem = new ItemStack(Material.GREEN_STAINED_GLASS);
                        ItemMeta activehologramItemMeta = activeHologramItem.getItemMeta();
                        activehologramItemMeta.setDisplayName("§aActivate Hologram");
                        activeHologramItem.setItemMeta(activehologramItemMeta);

                        event.getInventory().setItem(49, activeHologramItem);
                        break;
                    default:
                        //> Craft drugs
                        if(event.getCurrentItem().getItemMeta().getDisplayName().equals(" "))
                            return;

                        if(!event.getCurrentItem().getItemMeta().getLore().contains("    §a§lCRAFTABLE    "))
                            return;

                        File drugslabFile = new File("plugins/DrugsCore/drugslab.yml");
                        FileConfiguration drugslabFileCfg = YamlConfiguration.loadConfiguration(drugslabFile);
                        String druglabItemPath = DrugsCore.getInstance().getRecipesManager().findDruglabCraftingItemByName(event.getCurrentItem().getItemMeta().getDisplayName());
                        List<ItemStack> druglabCraftingItems = DrugsCore.getInstance().getRecipesManager().getDrugslabCraftingItems(druglabItemPath);

                        Material material = Material.matchMaterial(drugslabFileCfg.getString("recipes." + druglabItemPath + ".result.material"));
                        int amount = drugslabFileCfg.getInt("recipes." + druglabItemPath + ".result.amount");
                        String name = drugslabFileCfg.getString("recipes." + druglabItemPath + ".result.name");
                        ItemStack druglabItem = new ItemStack(material, amount);
                        ItemMeta druglabItemMeta = druglabItem.getItemMeta();
                        druglabItemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
                        druglabItem.setItemMeta(druglabItemMeta);

                        for(ItemStack druglabCraftingItem : druglabCraftingItems) {
                            int removingAmount = druglabCraftingItem.getAmount();

                            for(int i = 0; i < player.getInventory().getSize(); i++) {
                                try {
                                    if(druglabCraftingItem.getItemMeta().hasDisplayName()) {
                                        if(!player.getInventory().getItem(i).getItemMeta().getDisplayName().equals(druglabCraftingItem.getItemMeta().getDisplayName())) continue;
                                    } else {
                                        if(!player.getInventory().getItem(i).getType().equals(druglabCraftingItem.getType())) continue;
                                    }

                                    int newAmount = player.getInventory().getItem(i).getAmount() - removingAmount;
                                    if(newAmount >= 0) {
                                        player.getInventory().getItem(i).setAmount(newAmount);
                                        removingAmount = 0;
                                    } else {
                                        removingAmount = removingAmount - player.getInventory().getItem(i).getAmount();
                                        player.getInventory().getItem(i).setAmount(0);
                                    }

                                    if(removingAmount <= 0) break;
                                } catch(NullPointerException ex) {}
                            }
                        }

                        player.closeInventory();
                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully crafted " + ChatColor.translateAlternateColorCodes('&', name) + "§a!");
                        DrugsCore.getInstance().getItemHandler().giveItemToPlayer(player, druglabItem);
                        break;
                }
            }
        } catch(NullPointerException ex) {}
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();

        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Druglab"))
            DrugsCore.getInstance().getDruglabManager().getOpenedDruglabs().remove(player.getUniqueId());
    }
}
