package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.boosters.GlobalDrugBooster;
import solutions.misi.drugscore.boosters.RandomDrugBooster;
import solutions.misi.drugscore.plugin.DrugsCore;

public class ConfirmGUI implements Listener {

    public void open(Player player, String title) {
       Inventory gui = Bukkit.createInventory(null, 9, DrugsCore.getInstance().getMessage().getPrefix() + "§a§lConfirm §7- §e" + title);

       ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
       ItemMeta placeholderMeta = placeholder.getItemMeta();
       placeholderMeta.setDisplayName(" ");
       placeholder.setItemMeta(placeholderMeta);

       ItemStack confirm = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
       ItemMeta confirmMeta = confirm.getItemMeta();
       confirmMeta.setDisplayName("§a§lCONFIRM");
       confirm.setItemMeta(confirmMeta);

       ItemStack cancel = new ItemStack(Material.RED_STAINED_GLASS_PANE);
       ItemMeta cancelMeta = cancel.getItemMeta();
       cancelMeta.setDisplayName("§c§lCANCEL");
       cancel.setItemMeta(cancelMeta);

        gui.setItem(0, placeholder);
        gui.setItem(1, placeholder);
        gui.setItem(2, placeholder);
        gui.setItem(3, confirm);
        gui.setItem(4, placeholder);
        gui.setItem(5, cancel);
        gui.setItem(6, placeholder);
        gui.setItem(7, placeholder);
        gui.setItem(8, placeholder);

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        try {
            if(event.getView().getTitle().contains(DrugsCore.getInstance().getMessage().getPrefix() + "§a§lConfirm")) {
                event.setCancelled(true);

                if(event.getCurrentItem().getItemMeta().getDisplayName().equals("§a§lCONFIRM")) {
                    if(event.getView().getTitle().contains("Random Drug Booster")) {
                        RandomDrugBooster.activate(player, player.getInventory().getItemInMainHand());
                    } else if(event.getView().getTitle().contains("Global Drug Booster")) {
                        GlobalDrugBooster.activate(player, player.getInventory().getItemInMainHand());
                    } else if(event.getView().getTitle().toLowerCase().contains("formula")) {
                        int recipeNameStart = event.getView().getTitle().indexOf('-')+2;
                        String recipeName = event.getView().getTitle().substring(recipeNameStart);
                        String permission = DrugsCore.getInstance().getRecipesManager().getPermission(ChatColor.stripColor(recipeName));

                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully redeemed the §e" + recipeName + "§a!");
                        player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user " + player.getName() + " permission set " + permission + " true");
                    }

                    player.closeInventory();
                } else if(event.getCurrentItem().getItemMeta().getDisplayName().equals("§c§lCANCEL")) {
                    player.closeInventory();
                }
            }
        } catch(NullPointerException ex) {
            ex.printStackTrace();
        }
    }
}