package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugLevelsGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 36, DrugsCore.getInstance().getMessage().getPrefix() + "§9Drug Levels");

        ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta placeholderMeta = placeholder.getItemMeta();
        placeholderMeta.setDisplayName(" ");
        placeholder.setItemMeta(placeholderMeta);

        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);
        int currentPlayerLevel = DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId());

        for(int i = 0; i < gui.getSize(); i++) {
            if(i >= 11 && i <= 15)
                continue;
            if(i >= 20 && i <= 24)
                continue;

            gui.setItem(i, placeholder);
        }

        for(int i = 1; i < 11; i++) {
            if(currentPlayerLevel > i) {
                ItemStack previousLevel = new ItemStack(Material.BLACK_STAINED_GLASS);
                ItemMeta previousLevelMeta = previousLevel.getItemMeta();
                previousLevelMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + i + ".name")));
                List<String> previousLevelLore = new ArrayList<>();
                previousLevelLore.add(" ");
                previousLevelLore.add("§8§l⟲ PREVIOUS LEVEL");
                previousLevelLore.add(" ");
                previousLevelMeta.setLore(previousLevelLore);
                previousLevel.setItemMeta(previousLevelMeta);

                gui.addItem(previousLevel);
            } else if(currentPlayerLevel == i) {
                ItemStack currentLevel = new ItemStack(Material.GREEN_STAINED_GLASS);
                ItemMeta currentLevelMeta = currentLevel.getItemMeta();
                currentLevelMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + i + ".name")));
                List<String> currentLevelLore = new ArrayList<>();
                currentLevelLore.add(" ");
                currentLevelLore.add("§a§l✔ CURRENT LEVEL");
                currentLevelLore.add(" ");
                currentLevelMeta.setLore(currentLevelLore);
                currentLevel.setItemMeta(currentLevelMeta);

                gui.addItem(currentLevel);
            } else {
                ItemStack lockedLevel = new ItemStack(Material.RED_STAINED_GLASS);
                ItemMeta lockedLevelMeta = lockedLevel.getItemMeta();
                lockedLevelMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + i + ".name")));
                List<String> lockedLevelLore = new ArrayList<>();
                lockedLevelLore.add(" ");
                lockedLevelLore.add("§c§l✘ LOCKED LEVEL");
                lockedLevelLore.add("§7EXP Required: §f" + drugexpFileCfg.getDouble("levels." + i + ".xp-needed"));
                lockedLevelLore.add(" ");
                lockedLevelMeta.setLore(lockedLevelLore);
                lockedLevel.setItemMeta(lockedLevelMeta);

                gui.addItem(lockedLevel);
            }
        }

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Drug Levels"))
            event.setCancelled(true);
    }
}