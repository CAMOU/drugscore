package solutions.misi.drugscore.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.drugs.crafting.BlankFormula;
import solutions.misi.drugscore.drugs.crafting.Recipe;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FormulaShopGUI implements Listener {

    public void open(Player player) {
        Inventory gui = Bukkit.createInventory(null, 54, DrugsCore.getInstance().getMessage().getPrefix() + "§9Formulashop");

        ItemStack placeholder = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
        ItemMeta placeholderMeta = placeholder.getItemMeta();
        placeholderMeta.setDisplayName(" ");
        placeholder.setItemMeta(placeholderMeta);

        ItemStack playerRecipes = new ItemStack(Material.BOOK);
        ItemMeta playerRecipesMeta = playerRecipes.getItemMeta();
        playerRecipesMeta.setDisplayName("§bPurchased Recipes");
        List<String> playerRecipesLore = new ArrayList<>();
        playerRecipesLore.add(" ");
        playerRecipesLore.add("§7Click here to view all of");
        playerRecipesLore.add("§7your current purchased Recipes");
        playerRecipesLore.add(" ");
        playerRecipesMeta.setLore(playerRecipesLore);
        playerRecipes.setItemMeta(playerRecipesMeta);

        ItemStack drugs = new ItemStack(Material.WHEAT);
        ItemMeta drugsMeta = drugs.getItemMeta();
        drugsMeta.setDisplayName("§6Drugs");
        List<String> drugsLore = new ArrayList<>();
        drugsLore.add(" ");
        drugsLore.add("§7Click here to show");
        drugsLore.add("§7all available drugs");
        drugsLore.add(" ");
        drugsMeta.setLore(drugsLore);
        drugs.setItemMeta(drugsMeta);

        ItemStack drugsGuide = new ItemStack(Material.END_PORTAL_FRAME);
        ItemMeta drugsGuideMeta = drugsGuide.getItemMeta();
        drugsGuideMeta.setDisplayName("§eHow to craft Drugs?");
        List<String> drugsGuideLore = new ArrayList<>();
        drugsGuideLore.add(" ");
        drugsGuideLore.add("§7Start with going to the slums");
        drugsGuideLore.add("§7and find a dealer");
        drugsGuideLore.add(" ");
        drugsGuideMeta.setLore(drugsGuideLore);
        drugsGuide.setItemMeta(drugsGuideMeta);

        BlankFormula blankFormula = new BlankFormula();
        blankFormula.setAmount(1);
        ItemMeta blankFormulaMeta = blankFormula.getItemMeta();
        List<String> blankFormulaLore = new ArrayList<>();
        blankFormulaLore.add(" ");
        blankFormulaLore.add("§7Price: §b$" + blankFormula.getPrice());
        blankFormulaLore.add(" ");
        blankFormulaMeta.setLore(blankFormulaLore);
        blankFormula.setItemMeta(blankFormulaMeta);

        Map<Integer, ItemStack> recipes = DrugsCore.getInstance().getRecipesManager().getFormulaShopItems();

        for(int i = 0; i < gui.getSize(); i++)
            gui.setItem(i, placeholder);

        gui.setItem(1, playerRecipes);
        gui.setItem(2, drugs);
        gui.setItem(3, drugsGuide);
        gui.setItem(7, blankFormula);

        int recipeIndex = 19;
        for(int i = 0; i < recipes.size(); i++) {
            ItemStack recipe = recipes.get(i);

            if(!(recipeIndex >= 19 && recipeIndex <= 43))
                break;
            if(recipeIndex == 26 || recipeIndex == 35)
                recipeIndex += 2;
            gui.setItem(recipeIndex, recipe);

            recipeIndex++;
        }

        player.openInventory(gui);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        if(event.getView().getTitle().equals(DrugsCore.getInstance().getMessage().getPrefix() + "§9Formulashop")) {
            event.setCancelled(true);

            try {
                switch(event.getCurrentItem().getItemMeta().getDisplayName()) {
                    case "§bPurchased Recipes":
                        player.closeInventory();
                        DrugsCore.getInstance().getPurchasedRecipesGUI().open(player);
                        break;
                    case "§6Drugs":
                        player.closeInventory();
                        DrugsCore.getInstance().getDrugsGUI().open(player);
                        break;
                    default:
                        File recipesFile = new File("plugins/DrugsCore/recipes.yml");
                        FileConfiguration recipesFileCfg = YamlConfiguration.loadConfiguration(recipesFile);

                        if(event.getCurrentItem().getType() == Material.BOOK) {
                            String blankFormulaName = recipesFileCfg.getString("blank-formula.name");

                            int blankFormulaIndex = -1;
                            for(int i = 0; i < player.getInventory().getSize(); i++) {
                                if(player.getInventory().getItem(i) == null)
                                    continue;

                                if(player.getInventory().getItem(i).getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', blankFormulaName))) {
                                    blankFormulaIndex = i;
                                }
                            }

                            if(blankFormulaIndex == -1) {
                                player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cYou need a blank formula to buy this recipe!");
                                return;
                            }

                            Recipe recipe = new Recipe();
                            recipe.create(event.getCurrentItem().getItemMeta().getDisplayName(), 1);

                            DrugsCore.getInstance().getItemHandler().giveItemToPlayer(player, recipe);
                            if(player.getInventory().getItem(blankFormulaIndex).getAmount() == 1) {
                                player.getInventory().setItem(blankFormulaIndex, null);
                            } else {
                                player.getInventory().getItem(blankFormulaIndex).setAmount(player.getInventory().getItem(blankFormulaIndex).getAmount() - 1);
                            }

                            player.closeInventory();
                        } else if(event.getCurrentItem().getType() == Material.PAPER) {
                            double price = recipesFileCfg.getDouble("blank-formula.price");

                            if(DrugsCore.getInstance().getEconomy().getBalance(Bukkit.getOfflinePlayer(player.getUniqueId())) < price) {
                                player.closeInventory();
                                player.sendMessage(DrugsCore.getInstance().getMessage().getNotEnoughMoney());
                                return;
                            }

                            ItemStack blankFormula = new ItemStack(Material.PAPER);
                            ItemMeta blankFormulaMeta = blankFormula.getItemMeta();
                            blankFormulaMeta.setDisplayName(event.getCurrentItem().getItemMeta().getDisplayName());
                            blankFormula.setItemMeta(blankFormulaMeta);

                            DrugsCore.getInstance().getEconomy().withdrawPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()), price);
                            DrugsCore.getInstance().getItemHandler().giveItemToPlayer(player, blankFormula);
                        }

                        break;
                }
            } catch(NullPointerException ex) {}
        }
    }
}
