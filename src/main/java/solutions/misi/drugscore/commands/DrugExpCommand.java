package solutions.misi.drugscore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.drugs.Drug;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugExpCommand implements CommandExecutor {

    //> /drugexp
    //> /drugexp crops
    //> /drugexp levels
    //> /drugexp show <player>
    //> /drugexp autopick
    //>     drugscore.autopick
    //> /drugexp autoplant
    //>     drugscore.autoplant
    //> /drugxp fixlore

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;

        switch(args.length) {
            case 0:
                //> See all drugexp commands
                player.sendMessage("§7§m================§7[ §a§lDrugExp §7]§m================");
                player.sendMessage("§7/§adrugxp crops §7● See all drug crops and their xp worth");
                player.sendMessage("§7/§adrugxp levels §7● See all DrugExp levels");
                player.sendMessage("§7/§adrugxp show (player) §7● See info about current xp level");
                player.sendMessage("§7/§adrugxp autopick §7● Automatically put farmed crops into the inv");
                player.sendMessage("§7/§adrugxp autoplant §7● Automatically replant seeds");
                player.sendMessage("§7/§adrugxp fixlore §7● Fixes the drugs in your inventory");
                return true;
            case 1:
                if(args[0].equalsIgnoreCase("crops")) {
                    //> Open Crops GUI
                    DrugsCore.getInstance().getDrugCropsGUI().open(player);
                    return true;
                } else if(args[0].equalsIgnoreCase("levels")) {
                    //> Open Levels GUI
                    DrugsCore.getInstance().getDrugLevelsGUI().open(player);
                    return true;
                } else if(args[0].equalsIgnoreCase("show")) {
                    //> Shows drugxp, level and how much needed for next level from player
                    int currentPlayerLevel = DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId());
                    double currentPlayerExp = DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().get(player.getUniqueId());
                    double nextLevelExpRequired = DrugsCore.getInstance().getDrugExpHandler().getNextLevelExpNeeded(player);

                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7Your current level is: §a" + currentPlayerLevel);
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7Current Exp: §a" + Math.round(currentPlayerExp * 100.0) / 100.0);
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7Exp required for next level: §a" + nextLevelExpRequired);
                    return true;
                } else if(args[0].equalsIgnoreCase("autopick")) {
                    if(!player.hasPermission("drugscore.autopick")) {
                        player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                        return false;
                    }

                    //> Enables/Disables autopick-up of crops
                    if(DrugsCore.getInstance().getGadgetHandler().getEnabledAutopick().contains(player.getUniqueId())) {
                        DrugsCore.getInstance().getGadgetHandler().getEnabledAutopick().remove(player.getUniqueId());
                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cAutopickup has been disabled");
                    } else {
                        DrugsCore.getInstance().getGadgetHandler().getEnabledAutopick().add(player.getUniqueId());
                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aAutopickup has been enabled");
                    }
                    return true;
                } else if(args[0].equalsIgnoreCase("autoplant")) {
                    if(!player.hasPermission("drugscore.autoplant")) {
                        player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                        return false;
                    }

                    //> Enables/Disables autoplant of seeds
                    if(DrugsCore.getInstance().getGadgetHandler().getEnabledAutoplant().contains(player.getUniqueId())) {
                        DrugsCore.getInstance().getGadgetHandler().getEnabledAutopick().remove(player.getUniqueId());
                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cAutoplant has been disabled");
                    } else {
                        DrugsCore.getInstance().getGadgetHandler().getEnabledAutoplant().add(player.getUniqueId());
                        player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aAutoplant has been enabled");
                    }
                    return true;
                } else if(args[0].equalsIgnoreCase("fixlore")) {
                    //> Fixes lore of all drugs in players inventory
                    File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
                    FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);
                    String playerDrugLevelName = ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId()) + ".name"));

                    for(int i = 0; i < player.getInventory().getSize(); i++) {
                        try {
                            ItemStack item = player.getInventory().getItem(i).clone();

                            if (!item.getItemMeta().hasLore()) {
                                Drug drug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(item.getType());

                                ItemStack drugItem = new ItemStack(drug.getMaterial(), item.getAmount());
                                ItemMeta drugItemMeta = drugItem.getItemMeta();
                                drugItemMeta.setDisplayName(drug.getName());
                                List<String> drugLore = new ArrayList<>();
                                drugLore.add(" ");
                                drugLore.add("§7Drug Level: " + playerDrugLevelName);
                                drugLore.add(" ");
                                drugItemMeta.setLore(drugLore);
                                drugItem.setItemMeta(drugItemMeta);

                                player.getInventory().setItem(i, drugItem);
                            }
                        } catch(NullPointerException ex) {}
                    }

                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully fixed the lore of your drugs");
                    return true;
                } else {
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/drugexp §cfor help");
                }
            case 2:
                if(args[0].equalsIgnoreCase("show")) {
                    //> Shows drugxp, level and how much needed for next level from selected player
                    OfflinePlayer target = Bukkit.getPlayer(args[1]);

                    int currentPlayerLevel = DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(target.getUniqueId());
                    double currentPlayerExp = DrugsCore.getInstance().getDrugExpHandler().getDrugExpCache().get(target.getUniqueId());
                    double nextLevelExpRequired = DrugsCore.getInstance().getDrugExpHandler().getNextLevelExpNeeded(target.getPlayer());

                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7" + target.getName() + "'s current level is: §a" + currentPlayerLevel);
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7" + target.getName() + "'s current Exp: §a" + Math.round(currentPlayerExp * 100.0) / 100.0);
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§7Exp required for " + target.getName() + "'s next level: §a" + nextLevelExpRequired);
                    return true;
                } else {
                    player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/drugexp show <player>");
                }
            default:
                player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
        }

        return false;
    }
}
