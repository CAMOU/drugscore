package solutions.misi.drugscore.commands;

import lombok.SneakyThrows;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;

public class DrugpriceCommand implements CommandExecutor {

    //> /drugprice set <drug> <price>
    //>     drugscore.setdrugprice

    @SneakyThrows
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length == 3) {
            if(sender instanceof Player) {
                Player player = (Player) sender;

                if(!player.hasPermission("drugscore.setdrugprice")) {
                    player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                    return false;
                }
            }

            if(!args[0].equalsIgnoreCase("set")) {
                sender.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
                return false;
            }

            File drugsFile = new File(DrugsCore.getInstance().getDataFolder(), "drugs.yml");
            FileConfiguration drugsFileCfg = YamlConfiguration.loadConfiguration(drugsFile);

            String drugName = args[1].toLowerCase();
            double drugPrice = 0.0;

            try {
                drugPrice = Double.parseDouble(args[2]);
            } catch(NumberFormatException ex) {
                sender.sendMessage(DrugsCore.getInstance().getMessage().getWrongNumberUsage());
            }

            if(!drugsFileCfg.contains(drugName.toLowerCase())) {
                sender.sendMessage(DrugsCore.getInstance().getMessage().getDrugDoesntExist());
                return false;
            }

            drugsFileCfg.set(drugName.toLowerCase() + ".price", drugPrice);
            drugsFileCfg.save(drugsFile);

            sender.sendMessage(DrugsCore.getInstance().getMessage().getChangedDrugprice().replace("[price]", "" + drugPrice));
        } else {
            sender.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/drugprice set <drug> <price>");
        }

        return true;
    }
}
