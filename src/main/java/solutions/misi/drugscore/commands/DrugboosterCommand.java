package solutions.misi.drugscore.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.misi.drugscore.boosters.GlobalDrugBooster;
import solutions.misi.drugscore.boosters.RandomDrugBooster;
import solutions.misi.drugscore.plugin.DrugsCore;

public class DrugboosterCommand implements CommandExecutor {

    //> /drugbooster give <player> <booster name> <amount>
    //>     drugscore.givedrugbooster

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;

        if(args.length != 4) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/drugbooster give <player> <booster> <amount>");
            return false;
        }

        if(!player.hasPermission("drugscore.givedrugbooster")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
            return false;
        }

        if(!args[0].equalsIgnoreCase("give")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
            return false;
        }

        Player targetPlayer = Bukkit.getPlayer(args[1]);

        if(targetPlayer == null || !targetPlayer.isOnline()) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPlayerNotOnline());
            return false;
        }

        int amount;

        try {
            amount = Integer.parseInt(args[3]);
        } catch(NumberFormatException ex) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongNumberUsage());
            return false;
        }

        String booster = args[2].toLowerCase();

        if(booster.equals("random")) {
            RandomDrugBooster randomDrugBooster = new RandomDrugBooster();
            randomDrugBooster.setAmount(amount);
            DrugsCore.getInstance().getItemHandler().giveItemToPlayer(targetPlayer, randomDrugBooster);
            return true;
        } else if(booster.equals("global")) {
            GlobalDrugBooster globalDrugBooster = new GlobalDrugBooster();
            globalDrugBooster.setAmount(amount);
            DrugsCore.getInstance().getItemHandler().giveItemToPlayer(targetPlayer, globalDrugBooster);
            return true;
        } else {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cThere is no §6" + booster + " §cDrug Booster! Please use either general or random");
        }

        return false;
    }
}
