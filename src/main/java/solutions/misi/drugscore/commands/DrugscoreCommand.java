package solutions.misi.drugscore.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.misi.drugscore.drugs.crafting.druglab.Druglab;
import solutions.misi.drugscore.plugin.DrugsCore;

public class DrugscoreCommand implements CommandExecutor {

    //> /dcore give <player> <item> <amount>
    //>     drugscore.give.druglab

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;

        if(args.length != 4) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/dcore give <player> <item> <amount>");
            return false;
        }

        if(!args[0].equalsIgnoreCase("give")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
            return false;
        }

        Player targetPlayer = Bukkit.getPlayer(args[1]);

        if(targetPlayer == null || !targetPlayer.isOnline()) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPlayerNotOnline());
            return false;
        }

        int amount;

        try {
            amount = Integer.parseInt(args[3]);
        } catch(NumberFormatException ex) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongNumberUsage());
            return false;
        }

        String item = args[2].toLowerCase();

        switch(item) {
            case "druglab":
                if(!player.hasPermission("drugscore.give.druglab")) {
                    player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                    return false;
                }

                Druglab druglab = new Druglab();
                druglab.setAmount(amount);
                DrugsCore.getInstance().getItemHandler().giveItemToPlayer(targetPlayer, druglab);
                return true;
            default:
                player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cThere is no §6" + item + " §citem!");
                return false;
        }
    }
}