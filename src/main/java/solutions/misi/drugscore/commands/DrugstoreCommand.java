package solutions.misi.drugscore.commands;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import solutions.misi.drugscore.plugin.DrugsCore;

public class DrugstoreCommand implements CommandExecutor {

    //> /drugstore
    //>     drugscore.drugstore

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;

        if(args.length != 0) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
            return false;
        }

        if(!player.hasPermission("drugscore.drugstore")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
            return false;
        }

        //> Create Drugstore NPC
        NPC storeNPC = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "Dealer");
        storeNPC.setName("§6Drugs Buyer");
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "npc select " + storeNPC.getId());
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "npc skin Dealer");
        storeNPC.spawn(player.getLocation());

        return true;
    }
}
