package solutions.misi.drugscore.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import solutions.misi.drugscore.drugs.Drug;
import solutions.misi.drugscore.plugin.DrugsCore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DrugifyCommand implements CommandExecutor {

    //> /drugify condense/craft
    //>     drugscore.drugify.condense
    //>     drugscore.drugify.craft

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;
        File drugexpFile = new File("plugins/DrugsCore/drugexp.yml");
        FileConfiguration drugexpFileCfg = YamlConfiguration.loadConfiguration(drugexpFile);
        String playerDrugLevelName = ChatColor.translateAlternateColorCodes('&', drugexpFileCfg.getString("levels." + DrugsCore.getInstance().getDrugExpHandler().getDrugExpLevelCache().get(player.getUniqueId()) + ".name"));

        if(args.length != 1) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/drugify condense/craft");
            return false;
        }

        if(args[0].equalsIgnoreCase("condense")) {
            if(!player.hasPermission("drugscore.drugify.condense")) {
                player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                return false;
            }

            //> Condense drugs
            int foundWheat = 0;
            int foundNetherWarts = 0;
            int foundHoneycomb = 0;

            for(int i = 0; i < player.getInventory().getSize(); i++) {
                try {
                    if (player.getInventory().getItem(i).getType() == Material.WHEAT && player.getInventory().getItem(i).getItemMeta().hasLore()) {
                        foundWheat += player.getInventory().getItem(i).getAmount();
                        player.getInventory().getItem(i).setAmount(0);
                    }

                    if (player.getInventory().getItem(i).getType() == Material.NETHER_WART && player.getInventory().getItem(i).getItemMeta().hasLore()) {
                        foundNetherWarts += player.getInventory().getItem(i).getAmount();
                        player.getInventory().getItem(i).setAmount(0);
                    }

                    if (player.getInventory().getItem(i).getType() == Material.HONEYCOMB && player.getInventory().getItem(i).getItemMeta().hasLore()) {
                        foundHoneycomb += player.getInventory().getItem(i).getAmount();
                        player.getInventory().getItem(i).setAmount(0);
                    }
                } catch(NullPointerException ex) {
                }
            }

            //> Condense Wheat
            int remainingWheat;
            int hay;

            if(foundWheat >= 9) {
                remainingWheat = foundWheat % 9;
                hay = foundWheat / 9;
            } else {
                remainingWheat = foundWheat;
                hay = 0;
            }

            Drug hayDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.HAY_BLOCK);
            ItemStack hayItem = new ItemStack(Material.HAY_BLOCK, hay);
            ItemMeta hayItemMeta = hayItem.getItemMeta();
            hayItemMeta.setDisplayName(hayDrug.getName());
            List<String> hayItemLore = new ArrayList<>();
            hayItemLore.add(" ");
            hayItemLore.add("§7Drug Level: " + playerDrugLevelName);
            hayItemLore.add(" ");
            hayItemMeta.setLore(hayItemLore);
            hayItem.setItemMeta(hayItemMeta);

            Drug wheatDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.WHEAT);
            ItemStack wheatItem = new ItemStack(Material.WHEAT, remainingWheat);
            ItemMeta wheatItemMeta = wheatItem.getItemMeta();
            wheatItemMeta.setDisplayName(wheatDrug.getName());
            List<String> wheatItemLore = new ArrayList<>();
            wheatItemLore.add(" ");
            wheatItemLore.add("§7Drug Level: " + playerDrugLevelName);
            wheatItemLore.add(" ");
            wheatItemMeta.setLore(wheatItemLore);
            wheatItem.setItemMeta(wheatItemMeta);

            player.getInventory().addItem(hayItem);
            player.getInventory().addItem(wheatItem);

            //> Condense Netherwarts
            int remainingNetherWarts;
            int netherWartBlocks;

            if(foundNetherWarts >= 9) {
                remainingNetherWarts = foundNetherWarts % 9;
                netherWartBlocks = foundNetherWarts / 9;
            } else {
                remainingNetherWarts = foundNetherWarts;
                netherWartBlocks = 0;
            }

            Drug netherWartBlockDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.NETHER_WART_BLOCK);
            ItemStack netherWartBlockItem = new ItemStack(Material.NETHER_WART_BLOCK, netherWartBlocks);
            ItemMeta netherWartBlockItemMeta = netherWartBlockItem.getItemMeta();
            netherWartBlockItemMeta.setDisplayName(netherWartBlockDrug.getName());
            List<String> netherWartBlockItemLore = new ArrayList<>();
            netherWartBlockItemLore.add(" ");
            netherWartBlockItemLore.add("§7Drug Level: " + playerDrugLevelName);
            netherWartBlockItemLore.add(" ");
            netherWartBlockItemMeta.setLore(netherWartBlockItemLore);
            netherWartBlockItem.setItemMeta(netherWartBlockItemMeta);

            Drug netherWartDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.NETHER_WART);
            ItemStack netherWartItem = new ItemStack(Material.NETHER_WART, remainingNetherWarts);
            ItemMeta netherWartItemMeta = netherWartItem.getItemMeta();
            netherWartItemMeta.setDisplayName(netherWartDrug.getName());
            List<String> netherWartItemLore = new ArrayList<>();
            netherWartItemLore.add(" ");
            netherWartItemLore.add("§7Drug Level: " + playerDrugLevelName);
            netherWartItemLore.add(" ");
            netherWartItemMeta.setLore(netherWartItemLore);
            netherWartItem.setItemMeta(netherWartItemMeta);

            player.getInventory().addItem(netherWartBlockItem);
            player.getInventory().addItem(netherWartItem);

            //> Condense Honeycomb
            int remainingHoneycomb;
            int honeycombBlocks;

            if(foundNetherWarts >= 9) {
                remainingHoneycomb = foundHoneycomb % 4;
                honeycombBlocks = foundHoneycomb / 4;
            } else {
                remainingHoneycomb = foundHoneycomb;
                honeycombBlocks = 0;
            }

            Drug honeycombBlockDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.HONEYCOMB_BLOCK);
            ItemStack honeycombBlockItem = new ItemStack(Material.HONEYCOMB_BLOCK, honeycombBlocks);
            ItemMeta honeycombBlockItemMeta = honeycombBlockItem.getItemMeta();
            honeycombBlockItemMeta.setDisplayName(honeycombBlockDrug.getName());
            List<String> honeycombBlockItemLore = new ArrayList<>();
            honeycombBlockItemLore.add(" ");
            honeycombBlockItemLore.add("§7Drug Level: " + playerDrugLevelName);
            honeycombBlockItemLore.add(" ");
            honeycombBlockItemMeta.setLore(honeycombBlockItemLore);
            honeycombBlockItem.setItemMeta(honeycombBlockItemMeta);

            Drug honeycombDrug = DrugsCore.getInstance().getDrugsManager().findDrugByMaterial(Material.HONEYCOMB);
            ItemStack honeycombItem = new ItemStack(Material.HONEYCOMB, remainingHoneycomb);
            ItemMeta honeycombItemMeta = honeycombItem.getItemMeta();
            honeycombItemMeta.setDisplayName(honeycombDrug.getName());
            List<String> honeycombItemLore = new ArrayList<>();
            honeycombItemLore.add(" ");
            honeycombItemLore.add("§7Drug Level: " + playerDrugLevelName);
            honeycombItemLore.add(" ");
            honeycombItemMeta.setLore(honeycombItemLore);
            honeycombItem.setItemMeta(honeycombItemMeta);

            player.getInventory().addItem(honeycombBlockItem);
            player.getInventory().addItem(honeycombItem);

            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully condensed all drugs in your inventory!");
        } else if(args[0].equalsIgnoreCase("craft")) {
            if(!player.hasPermission("drugscore.drugify.craft")) {
                player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
                return false;
            }

            //> Craft Sugarcane into sugar
            int foundSugarcane = 0;
            for(int i = 0; i < player.getInventory().getSize(); i++) {
                try {
                    if(player.getInventory().getItem(i).getType() == Material.SUGAR_CANE) {
                        foundSugarcane += player.getInventory().getItem(i).getAmount();
                        player.getInventory().getItem(i).setAmount(0);
                    }
                } catch(NullPointerException ex) {}
            }

            player.getInventory().addItem(new ItemStack(Material.SUGAR, foundSugarcane));
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§aSuccessfully crafted all drugs in your inventory!");
        } else {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
            return false;
        }

        return true;
    }
}
