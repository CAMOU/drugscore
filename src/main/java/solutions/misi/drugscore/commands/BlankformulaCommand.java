package solutions.misi.drugscore.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import solutions.misi.drugscore.drugs.crafting.BlankFormula;
import solutions.misi.drugscore.plugin.DrugsCore;

public class BlankformulaCommand implements CommandExecutor {

    //> /blankformula give <player> <amount>
    //>     drugscore.giveblankformula

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)) {
            Bukkit.getConsoleSender().sendMessage("This command can only be executed in-game!");
            return false;
        }

        Player player = (Player) sender;

        if(args.length != 3) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPrefix() + "§cPlease use §a/blankformula give <player> <amount>");
            return false;
        }

        if(!player.hasPermission("drugscore.giveblankformula")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getNoPermission());
            return false;
        }

        if(!args[0].equalsIgnoreCase("give")) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongCommandUsage());
            return false;
        }

        Player targetPlayer = Bukkit.getPlayer(args[1]);

        if(targetPlayer == null || !targetPlayer.isOnline()) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getPlayerNotOnline());
            return false;
        }

        int amount;

        try {
            amount = Integer.parseInt(args[2]);
        } catch(NumberFormatException ex) {
            player.sendMessage(DrugsCore.getInstance().getMessage().getWrongNumberUsage());
            return false;
        }

        BlankFormula blankFormula = new BlankFormula();
        blankFormula.setAmount(amount);
        DrugsCore.getInstance().getItemHandler().giveItemToPlayer(targetPlayer, blankFormula);

        return true;
    }
}
