package solutions.misi.drugscore.plugin;

import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import solutions.misi.drugscore.boosters.GlobalDrugBooster;
import solutions.misi.drugscore.boosters.RandomDrugBooster;
import solutions.misi.drugscore.boosters.ShopBoosterHandler;
import solutions.misi.drugscore.commands.*;
import solutions.misi.drugscore.drugs.Consuming;
import solutions.misi.drugscore.drugs.DrugsManager;
import solutions.misi.drugscore.drugs.crafting.CraftingManager;
import solutions.misi.drugscore.drugs.crafting.Recipe;
import solutions.misi.drugscore.drugs.crafting.RecipesManager;
import solutions.misi.drugscore.drugs.crafting.druglab.Druglab;
import solutions.misi.drugscore.drugs.crafting.druglab.DruglabManager;
import solutions.misi.drugscore.drugs.drugexp.DrugExpHandler;
import solutions.misi.drugscore.gadgets.GadgetHandler;
import solutions.misi.drugscore.gui.*;
import solutions.misi.drugscore.mysql.DrugExpTable;
import solutions.misi.drugscore.mysql.DruglabsTable;
import solutions.misi.drugscore.mysql.MySQL;
import solutions.misi.drugscore.utils.ItemHandler;
import solutions.misi.drugscore.utils.Message;
import solutions.misi.drugscore.utils.Schedulers;

import java.io.File;

public class DrugsCore extends JavaPlugin {

    @Getter public static DrugsCore instance;

    @Getter private final HikariDataSource hikariDataSource = new HikariDataSource();
    @Getter private final MySQL mySQL = new MySQL();
    @Getter private final DruglabsTable druglabsTable = new DruglabsTable();
    @Getter private final DrugExpTable drugExpTable = new DrugExpTable();

    @Getter private final DrugsGUI drugsGUI = new DrugsGUI();
    @Getter private final DrugstoreGUI drugstoreGUI = new DrugstoreGUI();
    @Getter private final ConfirmGUI confirmGUI = new ConfirmGUI();
    @Getter private final FormulaShopGUI formulaShopGUI = new FormulaShopGUI();
    @Getter private final PurchasedRecipesGUI purchasedRecipesGUI = new PurchasedRecipesGUI();
    @Getter private final DruglabGUI drugslabGUI = new DruglabGUI();
    @Getter private final DrugCropsGUI drugCropsGUI = new DrugCropsGUI();
    @Getter private final DrugLevelsGUI drugLevelsGUI = new DrugLevelsGUI();

    @Getter private final DrugsManager drugsManager = new DrugsManager();
    @Getter private final ShopBoosterHandler shopBoosterHandler = new ShopBoosterHandler();
    @Getter private final Schedulers schedulers = new Schedulers();
    @Getter private final RecipesManager recipesManager = new RecipesManager();
    @Getter private final ItemHandler itemHandler = new ItemHandler();
    @Getter private final CraftingManager craftingManager = new CraftingManager();
    @Getter private final DruglabManager druglabManager = new DruglabManager();
    @Getter private final DrugExpHandler drugExpHandler = new DrugExpHandler();
    @Getter private final GadgetHandler gadgetHandler = new GadgetHandler();
    @Getter private final Consuming consuming = new Consuming();

    @Getter @Setter private Economy economy;
    @Getter private Message message;

    @SneakyThrows
    @Override
    public void onEnable() {
        instance = this;

        if(!setupEconomy()) {
            System.out.println("[DrugsCore] Could not enable the plugin due to dependencies not being installed. [Vault]");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        if(!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
            System.out.println("[DrugsCore] Could not enable the plugin due to dependencies not being installed. [HolographicDisplays]");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        saveDefaultConfig();

        //> Files
        File messagesFile = new File(getDataFolder(), "messages.yml");
        FileConfiguration messagesFileCfg = new YamlConfiguration();
        if(!messagesFile.exists())
            saveResource("messages.yml", false);
        messagesFileCfg.load(messagesFile);

        File drugsFile = new File(getDataFolder(), "drugs.yml");
        FileConfiguration drugsFileCfg = new YamlConfiguration();
        if(!drugsFile.exists())
            saveResource("drugs.yml", false);
        drugsFileCfg.load(drugsFile);

        File recipesFile = new File(getDataFolder(), "recipes.yml");
        FileConfiguration recipesFileCfg = new YamlConfiguration();
        if(!recipesFile.exists())
            saveResource("recipes.yml", false);
        recipesFileCfg.load(recipesFile);

        File craftingFile = new File(getDataFolder(), "crafting.yml");
        FileConfiguration craftingFileCfg = new YamlConfiguration();
        if(!craftingFile.exists())
            saveResource("crafting.yml", false);
        craftingFileCfg.load(craftingFile);

        File drugslabFile = new File(getDataFolder(), "drugslab.yml");
        FileConfiguration drugslabFileCfg = new YamlConfiguration();
        if(!drugslabFile.exists())
            saveResource("drugslab.yml", false);
        drugslabFileCfg.load(drugslabFile);

        File drugexpFile = new File(getDataFolder(), "drugexp.yml");
        FileConfiguration drugexpFileCfg = new YamlConfiguration();
        if(!drugexpFile.exists())
            saveResource("drugexp.yml", false);
        drugexpFileCfg.load(drugexpFile);

        File disguisesFile = new File(getDataFolder(), "disguises.yml");
        FileConfiguration disguisesFileCfg = new YamlConfiguration();
        if(!disguisesFile.exists())
            saveResource("disguises.yml", false);
        disguisesFileCfg.load(disguisesFile);

        message = new Message();
        mySQL.initializeDatabase();
        drugsManager.loadDrugsFromFile();
        craftingManager.loadCraftingRecipes();

        //> Schedulers
        schedulers.startAutosave();

        //> Commands
        DrugsCommand drugsCommand = new DrugsCommand();
        getCommand("drugs").setExecutor(drugsCommand);
        DrugpriceCommand drugpriceCommand = new DrugpriceCommand();
        getCommand("drugprice").setExecutor(drugpriceCommand);
        DrugstoreCommand drugstoreCommand = new DrugstoreCommand();
        getCommand("drugstore").setExecutor(drugstoreCommand);
        DrugboosterCommand drugboosterCommand = new DrugboosterCommand();
        getCommand("drugbooster").setExecutor(drugboosterCommand);
        FormulaShopCommand formulaShopCommand = new FormulaShopCommand();
        getCommand("formulashop").setExecutor(formulaShopCommand);
        BlankformulaCommand blankformulaCommand = new BlankformulaCommand();
        getCommand("blankformula").setExecutor(blankformulaCommand);
        DrugscoreCommand drugscoreCommand = new DrugscoreCommand();
        getCommand("drugscore").setExecutor(drugscoreCommand);
        DrugExpCommand drugExpCommand = new DrugExpCommand();
        getCommand("drugexp").setExecutor(drugExpCommand);
        DrugifyCommand drugifyCommand = new DrugifyCommand();
        getCommand("drugify").setExecutor(drugifyCommand);

        //> GUIs
        Bukkit.getPluginManager().registerEvents(new DrugsGUI(), this);
        Bukkit.getPluginManager().registerEvents(new DrugstoreGUI(), this);
        Bukkit.getPluginManager().registerEvents(new ConfirmGUI(), this);
        Bukkit.getPluginManager().registerEvents(new FormulaShopGUI(), this);
        Bukkit.getPluginManager().registerEvents(new PurchasedRecipesGUI(), this);
        Bukkit.getPluginManager().registerEvents(new DruglabGUI(), this);
        Bukkit.getPluginManager().registerEvents(new DrugCropsGUI(), this);
        Bukkit.getPluginManager().registerEvents(new DrugLevelsGUI(), this);

        //> Items
        Bukkit.getPluginManager().registerEvents(new RandomDrugBooster(), this);
        Bukkit.getPluginManager().registerEvents(new GlobalDrugBooster(), this);
        Bukkit.getPluginManager().registerEvents(new Recipe(), this);
        Bukkit.getPluginManager().registerEvents(new Druglab(), this);
        Bukkit.getPluginManager().registerEvents(new Consuming(), this);

        //> Other
        Bukkit.getPluginManager().registerEvents(new DrugExpHandler(), this);
    }

    @Override
    public void onDisable() {
        drugExpHandler.saveCacheData();
        schedulers.autosave.cancel();
        mySQL.closeConnection();
    }

    private boolean setupEconomy() {
        if(getServer().getPluginManager().getPlugin("Vault") == null)
            return false;
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if(rsp == null)
            return false;
        setEconomy(rsp.getProvider());
        return economy != null;
    }
}
